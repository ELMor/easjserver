package org.olmo.easjs.wbxml.eas.cp;

public class CPSearch extends CodePage {

	public static final String Search= "Search:Search";
	public static final String Store= "Search:Store";
	public static final String Name= "Search:Name";
	public static final String Query= "Search:Query";
	public static final String Options= "Search:Options";
	public static final String Range= "Search:Range";
	public static final String Status= "Search:Status";
	public static final String Response= "Search:Response";
	public static final String Result= "Search:Result";
	public static final String Properties= "Search:Properties";
	public static final String Total= "Search:Total";
	public static final String EqualTo= "Search:EqualTo";
	public static final String Value= "Search:Value";
	public static final String And= "Search:And";
	public static final String Or= "Search:Or";
	public static final String FreeText= "Search:FreeText";
	public static final String DeepTraversal= "Search:DeepTraversal";
	public static final String LongId= "Search:LongId";
	public static final String RebuildResults= "Search:RebuildResults";
	public static final String LessThan= "Search:LessThan";
	public static final String GreaterThan= "Search:GreaterThan";
	public static final String Schema= "Search:Schema";
	public static final String Supported= "Search:Supported";

	public CPSearch() {
		super(15,"Search");
        set(0x05 , Search);
        set(0x07 , Store);
        set(0x08 , Name);
        set(0x09 , Query);
        set(0x0A , Options);
        set(0x0B , Range);
        set(0x0C , Status);
        set(0x0D , Response);
        set(0x0E , Result);
        set(0x0F , Properties);
        set(0x10 , Total);
        set(0x11 , EqualTo);
        set(0x12 , Value);
        set(0x13 , And);
        set(0x14 , Or);
        set(0x15 , FreeText);
        set(0x17 , DeepTraversal);
        set(0x18 , LongId);
        set(0x19 , RebuildResults);
        set(0x1A , LessThan);
        set(0x1B , GreaterThan);
        set(0x1C , Schema);
        set(0x1D , Supported);
	}
}
