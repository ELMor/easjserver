package org.olmo.easjs.wbxml.eas.cp;

public class CPItemEstimate extends CodePage {

	public static final String GetItemEstimate="ItemEstimate:GetItemEstimate";
	public static final String Version="ItemEstimate:Version";
	public static final String Folders="ItemEstimate:Folders";
	public static final String Folder="ItemEstimate:Folder";
	public static final String FolderType="ItemEstimate:FolderType";
	public static final String FolderId="ItemEstimate:FolderId";
	public static final String DateTime="ItemEstimate:DateTime";
	public static final String Estimate="ItemEstimate:Estimate";
	public static final String Response="ItemEstimate:Response";
	public static final String Status="ItemEstimate:Status";

	public CPItemEstimate() {
		super(6,"ItemEstimate");
        set(0x05 , GetItemEstimate);
        set(0x06 , Version);
        set(0x07 , Folders);
        set(0x08 , Folder);
        set(0x09 , FolderType);
        set(0x0a , FolderId);
        set(0x0b , DateTime);
        set(0x0c , Estimate);
        set(0x0d , Response);
        set(0x0e , Status);
	}

}
