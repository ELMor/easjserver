package org.olmo.easjs.wbxml.eas.cp;

public class CPResolveRecipients extends CodePage {

	public static final String ResolveRecipients="ResolveRecipients:ResolveRecipients";
	public static final String Response="ResolveRecipients:Response";
	public static final String Status="ResolveRecipients:Status";
	public static final String Type="ResolveRecipients:Type";
	public static final String Recipient="ResolveRecipients:Recipient";
	public static final String DisplayName="ResolveRecipients:DisplayName";
	public static final String EmailAddress="ResolveRecipients:EmailAddress";
	public static final String Certificates="ResolveRecipients:Certificates";
	public static final String Certificate="ResolveRecipients:Certificate";
	public static final String MiniCertificate="ResolveRecipients:MiniCertificate";
	public static final String Options="ResolveRecipients:Options";
	public static final String To="ResolveRecipients:To";
	public static final String CertificateRetrieval="ResolveRecipients:CertificateRetrieval";
	public static final String RecipientCount="ResolveRecipients:RecipientCount";
	public static final String MaxCertificates="ResolveRecipients:MaxCertificates";
	public static final String MaxAmbiguousRecipients="ResolveRecipients:MaxAmbiguousRecipients";
	public static final String CertificateCount="ResolveRecipients:CertificateCount";

	public CPResolveRecipients() {
		super(10,"ResolveRecipients");
        set(0x05 , ResolveRecipients);
        set(0x06 , Response);
        set(0x07 , Status);
        set(0x08 , Type);
        set(0x09 , Recipient);
        set(0x0a , DisplayName);
        set(0x0b , EmailAddress);
        set(0x0c , Certificates);
        set(0x0d , Certificate);
        set(0x0e , MiniCertificate);
        set(0x0f , Options);
        set(0x10 , To);
        set(0x11 , CertificateRetrieval);
        set(0x12 , RecipientCount);
        set(0x13 , MaxCertificates);
        set(0x14 , MaxAmbiguousRecipients);
        set(0x15 , CertificateCount);
	}

}
