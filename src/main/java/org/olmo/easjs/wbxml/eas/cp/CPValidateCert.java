package org.olmo.easjs.wbxml.eas.cp;

public class CPValidateCert extends CodePage {

	public static final String ValidateCert="ValidateCert:ValidateCert";
	public static final String Certificates="ValidateCert:Certificates";
	public static final String Certificate="ValidateCert:Certificate";
	public static final String CertificateChain="ValidateCert:CertificateChain";
	public static final String CheckCRL="ValidateCert:CheckCRL";
	public static final String Status="ValidateCert:Status";

	public CPValidateCert() {
		super(11, "ValidateCert");
        set(0x05 , ValidateCert);
        set(0x06 , Certificates);
        set(0x07 , Certificate);
        set(0x08 , CertificateChain);
        set(0x09 , CheckCRL);
        set(0x0a , Status);
	}

}
