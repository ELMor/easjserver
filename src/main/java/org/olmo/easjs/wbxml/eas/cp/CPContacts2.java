package org.olmo.easjs.wbxml.eas.cp;

public class CPContacts2 extends CodePage {

	public static final String CustomerId="Contacts2:CustomerId";
	public static final String GovernmentId="Contacts2:GovernmentId";
	public static final String IMAddress="Contacts2:IMAddress";
	public static final String IMAddress2="Contacts2:IMAddress2";
	public static final String IMAddress3="Contacts2:IMAddress3";
	public static final String ManagerName="Contacts2:ManagerName";
	public static final String CompanyMainPhone="Contacts2:CompanyMainPhone";
	public static final String AccountName="Contacts2:AccountName";
	public static final String NickName="Contacts2:NickName";
	public static final String MMS="Contacts2:MMS";

	public CPContacts2() {
		super(12, "Contacts2");
        set(0x05 , CustomerId);
        set(0x06 , GovernmentId);
        set(0x07 , IMAddress);
        set(0x08 , IMAddress2);
        set(0x09 , IMAddress3);
        set(0x0a , ManagerName);
        set(0x0b , CompanyMainPhone);
        set(0x0c , AccountName);
        set(0x0d , NickName);
        set(0x0e , MMS);
	}

}
