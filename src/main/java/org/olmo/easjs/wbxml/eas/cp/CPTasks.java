package org.olmo.easjs.wbxml.eas.cp;

public class CPTasks extends CodePage {

	public static final String Body="Tasks:Body";
	public static final String BodySize="Tasks:BodySize";
	public static final String BodyTruncated="Tasks:BodyTruncated";
	public static final String Categories="Tasks:Categories";
	public static final String Category="Tasks:Category";
	public static final String Complete="Tasks:Complete";
	public static final String DateCompleted="Tasks:DateCompleted";
	public static final String DueDate="Tasks:DueDate";
	public static final String UtcDueDate="Tasks:UtcDueDate";
	public static final String Importance="Tasks:Importance";
	public static final String Recurrence="Tasks:Recurrence";
	public static final String Type="Tasks:Type";
	public static final String Start="Tasks:Start";
	public static final String Until="Tasks:Until";
	public static final String Occurrences="Tasks:Occurrences";
	public static final String Interval="Tasks:Interval";
	public static final String DayOfWeek="Tasks:DayOfWeek";
	public static final String DayOfMonth="Tasks:DayOfMonth";
	public static final String WeekOfMonth="Tasks:WeekOfMonth";
	public static final String MonthOfYear="Tasks:MonthOfYear";
	public static final String Regenerate="Tasks:Regenerate";
	public static final String DeadOccur="Tasks:DeadOccur";
	public static final String ReminderSet="Tasks:ReminderSet";
	public static final String ReminderTime="Tasks:ReminderTime";
	public static final String Sensitivity="Tasks:Sensitivity";
	public static final String StartDate="Tasks:StartDate";
	public static final String UtcStartDate="Tasks:UtcStartDate";
	public static final String Subject="Tasks:Subject";
	public static final String Rtf="Tasks:Rtf";

	public CPTasks() {
		super(9,"Tasks");
        set(0x05 , Body);
        set(0x06 , BodySize);
        set(0x07 , BodyTruncated);
        set(0x08 , Categories);
        set(0x09 , Category);
        set(0x0a , Complete);
        set(0x0b , DateCompleted);
        set(0x0c , DueDate);
        set(0x0d , UtcDueDate);
        set(0x0e , Importance);
        set(0x0f , Recurrence);
        set(0x10 , Type);
        set(0x11 , Start);
        set(0x12 , Until);
        set(0x13 , Occurrences);
        set(0x14 , Interval);
        set(0x16 , DayOfWeek);
        set(0x15 , DayOfMonth);
        set(0x17 , WeekOfMonth);
        set(0x18 , MonthOfYear);
        set(0x19 , Regenerate);
        set(0x1a , DeadOccur);
        set(0x1b , ReminderSet);
        set(0x1c , ReminderTime);
        set(0x1d , Sensitivity);
        set(0x1e , StartDate);
        set(0x1f , UtcStartDate);
        set(0x20 , Subject);
        set(0x21 , Rtf);
	}

}
