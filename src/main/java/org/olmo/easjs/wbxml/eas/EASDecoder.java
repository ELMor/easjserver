package org.olmo.easjs.wbxml.eas;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.olmo.common.Util;
import org.olmo.easjs.Conf;
import org.olmo.easjs.wbxml.DH;
import org.olmo.easjs.wbxml.Debug;
import org.olmo.easjs.wbxml.Parser;
import org.olmo.easjs.wbxml.eas.cp.CodePage;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class EASDecoder extends Parser {
	DH docHandler=null;
	Document doc=null;
	XPath xpHandler=null;
	String cont=null;
	byte input[]=null;
	Logger log=Logger.getLogger("org.olmo.easjs.wbxml");
	
	public EASDecoder(InputStream is) throws SAXException, IOException, ParserConfigurationException{
		setTagTable(CodePage.codePages);
		docHandler=new DH();
		setDocumentHandler(docHandler);
		byte buffer[]=Util.inputStreamToByteArray(is);
		if(Conf.debug){
			String fn=Debug.dumpFile("fromPDA.bin", buffer);
			log.info(" Received command-->"+fn+" Decoding");
		}
		is=new ByteArrayInputStream(buffer);
		SAXException exc=null;
		//Ping puede enviar stream vacio
		if(buffer.length>0){
			parse(is);
			try {
				doc=buildDoc();
			}catch(SAXException se){
				exc=se;
			}
		}
		if(Conf.debugXmlStreams){
			if(exc==null && cont!=null){
				Debug.dumpFile("fromPDA.xml", cont, true);
			}
		}
	}
	
	public Document getDoc() {
		return doc;
	}
	
	public String getXML(){
		return cont;
	}
	
	public Document buildDoc() throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = dbf.newDocumentBuilder();
		cont=docHandler.xml.toString();
		byte bytes[]=cont.getBytes();
		ByteArrayInputStream baos=new ByteArrayInputStream(bytes);
		Document document = builder.parse(baos);
		return document;
	}
	
	public int xprNumber(String run) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException{
		if(xpHandler==null)
			xpHandler=XPathFactory.newInstance().newXPath();
		return (Integer) xpHandler.evaluate(run, getDoc(), XPathConstants.NUMBER);
	}
	
	public NodeList xprNodeList(String ...arg) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException{
		String xpath="";
		for(int i=0;i<arg.length;i++){
			if(xpath.length()>0)
				xpath+="/";
			int ndx=arg[i].indexOf(':');
			if(ndx>0)
				xpath+=arg[i].substring(ndx+1);
			else
				xpath+=arg[i];
		}
		if(xpHandler==null)
			xpHandler=XPathFactory.newInstance().newXPath();
		return (NodeList) xpHandler.evaluate(xpath, getDoc(), XPathConstants.NODESET);
	}
	
	public Node xprNode(String run) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException{
		if(xpHandler==null)
			xpHandler=XPathFactory.newInstance().newXPath();
		return (Node) xpHandler.evaluate(
				run, getDoc(), XPathConstants.NODE);
	}
	
	public String xprText(String run) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException{
		if(xpHandler==null)
			xpHandler=XPathFactory.newInstance().newXPath();
		return (String) xpHandler.evaluate(
				run, getDoc(), XPathConstants.STRING);
	}
}
