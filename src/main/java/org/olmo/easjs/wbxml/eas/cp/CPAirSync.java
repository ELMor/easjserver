package org.olmo.easjs.wbxml.eas.cp;

public class CPAirSync extends CodePage {

	public static final String Synchronize="AirSync:Synchronize";
	public static final String Responses="AirSync:Responses";
	public static final String Add="AirSync:Add";
	public static final String Change="AirSync:Change";
	public static final String Delete="AirSync:Delete";
	public static final String Fetch="AirSync:Fetch";
	public static final String SyncKey="AirSync:SyncKey";
	public static final String ClientEntryId="AirSync:ClientEntryId";
	public static final String ServerEntryId="AirSync:ServerEntryId";
	public static final String Status="AirSync:Status";
	public static final String Collection="AirSync:Collection";
	public static final String Class="AirSync:Class";
	public static final String Version="AirSync:Version";
	public static final String CollectionId="AirSync:CollectionId";
	public static final String GetChanges="AirSync:GetChanges";
	public static final String MoreAvailable="AirSync:MoreAvailable";
	public static final String MaxItems="AirSync:MaxItems";
	public static final String Commands="AirSync:Commands";
	public static final String Options="AirSync:Options";
	public static final String FilterType="AirSync:FilterType";
	public static final String Truncation="AirSync:Truncation";
	public static final String RtfTruncation="AirSync:RtfTruncation";
	public static final String Conflict="AirSync:Conflict";
	public static final String Collections="AirSync:Collections";
	public static final String Data="AirSync:Data";
	public static final String DeletesAsMoves="AirSync:DeletesAsMoves";
	public static final String NotifyGUID="AirSync:NotifyGUID";
	public static final String Supported="AirSync:Supported";
	public static final String SoftDelete="AirSync:SoftDelete";
	public static final String MIMESupport="AirSync:MIMESupport";
	public static final String MIMETruncation="AirSync:MIMETruncation";
	public static final String NewMessage="AirSync:NewMessage";

	public CPAirSync() {
		super(0,"AirSync");
		set(0x05,  Synchronize);
        set(0x06 , Responses);
        set(0x07 , Add);
        set(0x08 , Change);
        set(0x09 , Delete);
        set(0x0a , Fetch);
        set(0x0b , SyncKey);
        set(0x0c , ClientEntryId);
        set(0x0d , ServerEntryId);
        set(0x0e , Status);
        set(0x0f , Collection);
        set(0x10 , Class);
        set(0x11 , Version);
        set(0x12 , CollectionId);
        set(0x13 , GetChanges);
        set(0x14 , MoreAvailable);
        set(0x15 , MaxItems);
        set(0x16 , Commands);
        set(0x17 , Options);
        set(0x18 , FilterType);
        set(0x19 , Truncation);
        set(0x1a , RtfTruncation);
        set(0x1b , Conflict);
        set(0x1c , Collections);
        set(0x1d , Data);
        set(0x1e , DeletesAsMoves);
        set(0x1f , NotifyGUID);
        set(0x20 , Supported);
        set(0x21 , SoftDelete);
        set(0x22 , MIMESupport);
        set(0x23 , MIMETruncation);
	}
	
}
