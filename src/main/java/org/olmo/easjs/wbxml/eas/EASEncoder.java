package org.olmo.easjs.wbxml.eas;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Logger;

import org.olmo.easjs.Conf;
import org.olmo.easjs.server.object.SyncObject;
import org.olmo.easjs.wbxml.Constantes;
import org.olmo.easjs.wbxml.Debug;
import org.olmo.easjs.wbxml.eas.cp.CodePage;

public class EASEncoder implements Constantes {
	static final String blanks="                                                            ";
	static Logger log=Logger.getLogger("org.olmo.easjs.server.MSASHandler");;
	
	StringBuffer sb=new StringBuffer();
	Stack<String> openedTags=new Stack<String>();
	String conditionalOpenedTag=null;
	ByteArrayOutputStream debugBuffer=new ByteArrayOutputStream();
	boolean withContent=false;

	CodePage active=CodePage.getCodePage("AirSync"); //Default
	CodePage requested=null;
	OutputStream toPDA=null;
	
	public EASEncoder(String pn, OutputStream os){
		requested=CodePage.getCodePage(pn);
		toPDA=os;
		write(0x03); //Version
		write(0x01); //Unknown or missing public id
		write(0x6A); //iso-8859-1
		writeInt(0); //String table size
	}
		
	public String norm(String tag){
		int ndx=tag.indexOf(':');
		if(ndx>=0){
			//Comprobamos que el codigo de pagina es el mismo
			String nspace=tag.substring(0, ndx);
			requested=CodePage.getCodePage(nspace);
			return tag.substring(ndx+1);
		}else{
			requested=CodePage.getCodePage("AirSync");
			return tag;
		}
	}
	
	public void openCond(String ctag){
		if(conditionalOpenedTag!=null){
			open(conditionalOpenedTag);
		}
		conditionalOpenedTag=ctag;
	}
	
	public void open(String ... tags){
		if(conditionalOpenedTag!=null){
			String dummy=conditionalOpenedTag;
			conditionalOpenedTag=null;
			open(dummy);
		}
		for(int i=0;i<tags.length;i++){
			//StringBuffer
			String tag=norm(tags[i]);
			sb.append("\n");
			sb.append(blanks.substring(0,openedTags.size()));
			sb.append("<").append(tag).append(">");
			openedTags.push(tag);
			withContent=false;
			if(requested!=null && active.getCode()!=requested.getCode()){
				active=requested;
				write(SWITCH_PAGE);
				write(active.getCode());
			}
			write(0x40|active.get(tag));
		}
	}
	
	public void chars(String cnt) {
		if(conditionalOpenedTag!=null){
			open(conditionalOpenedTag);
			conditionalOpenedTag=null;
		}
		//StringBuffer
		withContent=true;
		sb.append(cnt);
		//parsingBuffer
		write(STR_I);
		writeStrI(cnt);
	}

	public void close(){
		if(conditionalOpenedTag!=null){
			conditionalOpenedTag=null;
			return;
		}
		//StringBuffer
		String tag=openedTags.pop();
		if(!withContent){
			sb.append("\n");
			sb.append(blanks.substring(0,openedTags.size()));
		}
		sb.append("</").append(tag).append(">");
		withContent=false;
		//parsingBuffer
		write(END);
	}
	
	public void openclose(String tag){
		open(tag);
		close();
	}
	
	public void element(String tag, SyncObject content){
		if(content==null)
			return;
		open(tag);
		content.encode(this);
		close();
	}
	
	public void element(String tag, boolean ok){
		open(tag);
		chars(ok?"1":"0");
		close();
	}
	
	public void element(String tag, Vector<SyncObject> els){
		if(els==null || els.size()==0)
			return;
		for(int i=0;i<els.size();i++)
			element(tag,els.get(i));
	}
	
	public void element(String ...tags){
		for(int i=0;i<tags.length;i+=2){
			String tag=tags[i];
			String content=tags[i+1];
			if(content!=null && content.length()>0){
				open(tag);
				chars(content);
				close();
			}
		}
	}
	
	public byte[] getBytes(){
		try {
			byte b[]=debugBuffer.toByteArray();
			if(Conf.debug){
				String fn=Debug.dumpFile("toPDA.bin", b);
				log.info("Response -->"+fn+" ");
			}
			if(Conf.debugXmlStreams){
				Debug.dumpFile("toPDA.xml", sb.toString(), false);
			}
			return new String(b).getBytes("UTF-8");
		}catch(IOException e){
			e.printStackTrace();
			return null;
		}
		
		
	}

	public String getStream(){
		if(Conf.debugXmlStreams){
			log.info(sb.toString());
		}
		return sb.toString();
	}
	
	private void writeStrI(String cnt) {
		try {
			write(cnt.getBytes("UTF-8"));
			write(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void writeInt(int numberToWrite){
		writeInt(toPDA,numberToWrite);
		writeInt(debugBuffer,numberToWrite);
	}
	
	private void write(byte b[]){
		try {
			debugBuffer.write(b);
			toPDA.write(b);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void write(int b){
		try {
			toPDA.write(b);
			debugBuffer.write(b);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	private void writeInt(OutputStream out, int numberToWrite) {
		byte[] buf = new byte[5];
		int idx = 0;

		do {
			buf[idx++] = (byte) (numberToWrite & 0x7f);
			numberToWrite = numberToWrite >> 7;
		} while (numberToWrite != 0);

		try {
			while (idx > 1) {
				out.write(buf[--idx] | 0x80);
			}
			out.write(buf[0]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
