package org.olmo.easjs.wbxml.eas.cp;

public class CPMeetingResponse extends CodePage {

	public static final String CalendarId="MeetingResonse:CalendarId";
	public static final String FolderId="MeetingResonse:FolderId";
	public static final String MeetingResponse="MeetingResonse:MeetingResponse";
	public static final String RequestId="MeetingResonse:RequestId";
	public static final String Request="MeetingResonse:Request";
	public static final String Result="MeetingResonse:Result";
	public static final String Status="MeetingResonse:Status";
	public static final String UserResponse="MeetingResonse:UserResponse";
	public static final String Version="MeetingResonse:Version";

	public CPMeetingResponse() {
		super(8,"MeetingResonse");
        set(0x05 , CalendarId);
        set(0x06 , FolderId);
        set(0x07 , MeetingResponse);
        set(0x08 , RequestId);
        set(0x09 , Request);
        set(0x0a , Result);
        set(0x0b , Status);
        set(0x0c , UserResponse);
        set(0x0d , Version);
	}

}
