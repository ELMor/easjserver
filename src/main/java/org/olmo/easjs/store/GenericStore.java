package org.olmo.easjs.store;

import java.io.Serializable;

public interface GenericStore {
	public boolean login(String user, String password, String url);
	public boolean logout();
	public Object get(String key);
	public boolean put(String key, Serializable value);
	public boolean remove(String key);
	public String getSrvIdFromBendId(String user, String backendId);
	public String getBendIdFromSrvId(String user, String srvId);
}
