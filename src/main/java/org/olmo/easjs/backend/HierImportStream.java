package org.olmo.easjs.backend;

import java.util.Vector;

import org.olmo.easjs.server.object.SyncFolder;

public class HierImportStream  extends HierImport {
	private Vector<SyncFolder> changed;
	private Vector<SyncFolder> deleted;
	
	public HierImportStream(Backend b) {
		super(b);
		this.changed = new Vector<SyncFolder>();
		this.deleted = new Vector<SyncFolder>();
	}

	
	public String folderChange(SyncFolder folder) {
		changed.add(folder);
		return folder.id;
	}

	
	public String folderDeletion(SyncFolder folder) {
		deleted.add(folder);
		return folder.id;
	}

	public int getSize(){
		return changed.size()+deleted.size();
	}
	
	public Vector<SyncFolder> getChanged(){
		return changed;
	}
	public Vector<SyncFolder> getDeleted(){
		return deleted;
	}
}
