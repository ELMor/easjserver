package org.olmo.easjs.backend.impl.google;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.olmo.common.Util;
import org.olmo.easjs.backend._IObjectGetter;
import org.olmo.easjs.backend._IObjectSetter;
import org.olmo.easjs.server.object.SyncContact;
import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncObject;

import com.google.gdata.client.Query;
import com.google.gdata.client.Service.GDataRequest;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.Link;
import com.google.gdata.data.contacts.Birthday;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.contacts.Website;
import com.google.gdata.data.extensions.Email;
import com.google.gdata.data.extensions.FamilyName;
import com.google.gdata.data.extensions.GivenName;
import com.google.gdata.data.extensions.Name;
import com.google.gdata.data.extensions.PhoneNumber;
import com.google.gdata.util.ContentType;
import com.google.gdata.util.ServiceException;

public class ContactManager implements _IObjectGetter,_IObjectSetter {

	public static final String serviceUrlHeader="http://www.google.com/m8/feeds/contacts/";
	ContactsService service=null;
	String srvUrl,user,password;
	private SyncFolder contactFolder=new SyncFolder();
	Logger log=Logger.getLogger("org.olmo.easjs.backend.impl.google.ContactManager");
	
	public ContactManager(String usr, String pass) throws Exception {
		contactFolder.displayname="Contactos";
		contactFolder.id=FOLDER_TYPE_CONTACT_ID;
		contactFolder.mod="NOMOD";
		contactFolder.type=""+FOLDER_TYPE_CONTACT;
		contactFolder.parentid="0";
		contactFolder.read="1";
		user=usr;
		password=pass;
		srvUrl=serviceUrlHeader+user+"/full";
		service=new ContactsService("EASjServer");
		service.setUserCredentials(user,password);
	}
	
	
	public SyncFolder folderGet(String fid) {
		if(fid.equals(contactFolder.id))
			return contactFolder;
		return null;
	}

	
	public SyncObject objectGet(String fid, String id, int truncSize) {
		try {
			URL queryUrl=new URL(serviceUrlHeader+id);
			ContactEntry ce=service.getEntry(queryUrl, ContactEntry.class);
			SyncContact contact=new SyncContact(null);
			contact.id=id;
			contact.mod=ce.getEtag();
			fromContactEntryToSyncContact(ce, contact);
			return contact;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public Vector<SyncObject> folderList() {
		Vector<SyncObject> ret=new Vector<SyncObject>();
		ret.add(contactFolder);
		return ret;
	}

	
	public SyncObject folderStat(String fid) {
		return folderGet(fid);
	}

	
	public Vector<SyncObject> objectList(String fid, int cutOffDate) {
		Vector<SyncObject> ret = new Vector<SyncObject>();
		try {
			//Contacts
			URL qu=new URL(srvUrl);
			Query q=new Query(qu);
			//q.setPublishedMin(new DateTime(1));
			ContactFeed cf=service.query(q, ContactFeed.class);
			List<ContactEntry> ces = cf.getEntries();
			for(int i=0;i<ces.size();i++){
				ContactEntry ce=ces.get(i);
				SyncObject add=fromContactEntryToStat(ce);
				ret.add(add);
			}
		}catch(Exception e){
			e.printStackTrace();
			ret=null;
		}
		return ret;
	}

	private SyncObject fromContactEntryToStat(ContactEntry ce){
		if(ce!=null){
			SyncObject c=new SyncObject();
			c.id=ce.getId().substring(serviceUrlHeader.length());
			c.mod=ce.getEtag();
			c.flags="0";
			c.read="0";
			return c;
		}
		return null;
	}
	
	
	public SyncObject objectStat(String fid, String id) {
		try {
			URL url=new URL(serviceUrlHeader+id);
			ContactEntry ce=service.getEntry(url, ContactEntry.class);
			return fromContactEntryToStat(ce);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public SyncObject folderChange(String parent, String oldid, String newid) {
		return null; //No permitido
	}

	
	public boolean folderDelete(String fid) {
		return false; //No permitido
	}

	
	public SyncObject objectChange(String fid, String id, SyncObject nuevo) {
		try {
			SyncContact message=(SyncContact)nuevo;
			ContactEntry ce=null;
			if(id==null){ //Crear
				ce=new ContactEntry();
				fromSyncContactToContactEntry(message, ce);
				URL u=new URL(srvUrl);
				ce=service.insert(u, ce);
			}else{ //Modificar
				URL queryUrl=new URL(serviceUrlHeader+id);
				ce=service.getEntry(queryUrl, ContactEntry.class);
				fromSyncContactToContactEntry((SyncContact) message, ce);
				URL eu=new URL(ce.getEditLink().getHref());
				ce=service.update(eu, ce);
			}
			return objectStat(fid, ce.getId().substring(serviceUrlHeader.length()));
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	
	public boolean objectDelete(String fid, String id) {
		try {
			URL qu=new URL(serviceUrlHeader+id);
			ContactEntry ce=service.getEntry(qu, ContactEntry.class);
			ce.delete();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	
	public boolean objectMove(String fid, String id, String newFid) {
		return false; //No permitido �Donde moverlo?
	}

	private void setGoogleEmail(String address, ContactEntry out, String type) {
		if(address==null)
			return;
		boolean found=false;
		Email emailAddress=new Email();
		List<Email> eml=out.getEmailAddresses();
		for(int i=0;i<eml.size();i++){
			Email em=eml.get(i);
			if(em.getLabel().equals(type)){
				found=true;
				emailAddress=em;
				break;
			}
		}
		emailAddress.setAddress(address);
		emailAddress.setLabel(type);
		if(!found)
			out.addEmailAddress(emailAddress);
	}

	private void setGooglePhoneNumber(String number, ContactEntry out,String type) {
		if(number==null)
			return;
		List<PhoneNumber> pnl=out.getPhoneNumbers();
		PhoneNumber phoneNumber=new PhoneNumber();
		boolean found=false;
		for(int i=0;i<pnl.size();i++){
			PhoneNumber pn=pnl.get(i);
			if(pn.getLabel().equals(type)){
				phoneNumber=pn;
				found=true;
				break;
			}
		}
		phoneNumber.setLabel(type);
		phoneNumber.setPhoneNumber(number);
		if(!found)
			out.addPhoneNumber(phoneNumber);
	}

	private void fromSyncContactToContactEntry(SyncContact in, ContactEntry out){
		if(in.firstname!=null || in.lastname!=null || in.middlename!=null ){
			
			Name n=new Name();
			if(in.firstname!=null){
				GivenName gn=new GivenName(in.firstname,null);
				n.setGivenName(gn);
			}
			if(in.lastname!=null){
				FamilyName fn=new FamilyName(in.lastname,null);
				n.setFamilyName(fn);
			}
			out.setName(n);
		}

		if(in.birthday!=null){
			Birthday birthday=new Birthday(in.birthday.toString());
			out.setBirthday(birthday);
		}
		if(in.picture!=null){
			Base64 dec=new Base64();
			try {
				byte pic[]=dec.decode(in.picture);
			    Link photoLink = out.getContactEditPhotoLink();
			    if(photoLink!=null){
				    URL photoUrl = new URL(photoLink.getHref());
				    GDataRequest request = service.createRequest(
				    		GDataRequest.RequestType.UPDATE,
				    		photoUrl, 
				    		new ContentType("image/jpeg"));
				    OutputStream requestStream = request.getRequestStream();
				    requestStream.write(pic);
				    request.execute();
			    }else{
			    	log.info("ContactEditPhotoLink is null. Cancelando actualizacion.");
			    }
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		setGooglePhoneNumber(in.homephonenumber, out, "Casa");
		setGooglePhoneNumber(in.businessphonenumber, out, "Trabajo");
		setGooglePhoneNumber(in.home2phonenumber, out, "Casa - Otro");
		setGooglePhoneNumber(in.mobilephonenumber, out, "Movil");
		setGooglePhoneNumber(in.business2phonenumber, out, "Trabajo - Otro");
		setGooglePhoneNumber(in.carphonenumber, out, "Coche");
		setGooglePhoneNumber(in.pagernumber, out, "Localizador");
		setGooglePhoneNumber(in.businessfaxnumber, out, "Trabajo - Fax");
			
		setGoogleEmail(in.email1address, out, "Trabajo");
		setGoogleEmail(in.email2address, out, "Personal");
		setGoogleEmail(in.email3address, out, "Otro");

	}
	private void fromContactEntryToSyncContact(ContactEntry in, SyncContact out) {
		if(in.hasPhoneNumbers()){
			List<PhoneNumber> pnl=in.getPhoneNumbers();
			for(int i=0;i<pnl.size();i++){
				PhoneNumber pn=pnl.get(i);
				String lbl=pn.getLabel();
				String p=pn.getPhoneNumber();
				if(lbl==null){
					out.mobilephonenumber=p;
				}else if(lbl.equals("Casa")){
					out.homephonenumber=p;
				}else if(lbl.equals("Trabajo")){
					out.businessphonenumber=p;
				}else if(lbl.equals("Casa - Otro")){
					out.home2phonenumber=p;
				}else if(lbl.equals("Movil")){
					out.mobilephonenumber=p;
				}else if(lbl.equals("Trabajo - Otro")){
					out.business2phonenumber=p;
				}else if(lbl.equals("Coche")){
					out.carphonenumber=p;
				}else if(lbl.equals("Localizador")){
					out.pagernumber=p;
				}else if(lbl.equals("Trabajo - Fax")){
					out.businessfaxnumber=p;
				}
			}
		}
		if(in.hasEmailAddresses()){
			List<Email> emailList=in.getEmailAddresses();
			for(int i=0;i<emailList.size();i++){
				Email e=emailList.get(i);
				String lbl=e.getLabel();
				String p=e.getAddress();
				if(lbl==null)
					out.email1address=p;
				else if(lbl.equals("Trabajo"))
					out.email1address=p;
				else if(lbl.equals("Personal"))
					out.email2address=p;
				else if(lbl.equals("Otro"))
					out.email3address=p;
								
			}
		}
		
		if(in.hasWebsites()){
			List<Website> wsl=in.getWebsites();
			for(int i=0;i<wsl.size();i++){
				Website ws=wsl.get(i);
				String p=ws.getHref();
				out.webpage=p;
			}
		}
		
		if(in.hasBirthday()){
			String b=in.getBirthday().getWhen();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			try {
				out.birthday=sdf.parse(b);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if(in.hasName()){
			out.firstname=in.getName().getGivenName().getValue();
			out.lastname=in.getName().getFamilyName().getValue();
			out.fileas=out.firstname+" "+out.lastname;
		}
		
		if(in.hasWhere()){
			//TODO hasWhere in Contacts Completar
		}
		if(in.getContactPhotoLink()!=null){
			try {
				Link link=in.getContactPhotoLink();
				URL getter=new URL(link.getHref());
				InputStream is=getter.openStream();
				byte buf[]=Util.inputStreamToByteArray(is);
				is.close();
				out.picture=new String(new Base64().encode(buf));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	
	public byte[] getAttachment(String attname) {
		return null; //No hay attachments en contactos
	}

	
	public boolean sendMail(String rfc822, boolean forward, boolean reply,
			String parent) {
		return false; //No tiene sentido para contactos
	}

	
	public boolean setReadFlag(String fid, String id, String flags) {
		return false; //No tiene sentido para contactos
	}

	
	public boolean monitorFolder(String fid) {
		return false;
	}

	
	public Vector<SyncObject> getMonitorizedChanges(String fid) {
		return null;
	}
	

	
}
