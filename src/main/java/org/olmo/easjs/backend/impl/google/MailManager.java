package org.olmo.easjs.backend.impl.google;

import org.olmo.easjs.backend.impl.IMAPManager;

public class MailManager extends IMAPManager {

	public MailManager(String imapUsr, String imapPwd) {
		super(
				imapUsr, imapPwd, "imap.gmail.com", "imaps", 
				imapUsr, imapPwd, "smtp.gmail.com", "smtps", "465", true, true
			);
	}

}
