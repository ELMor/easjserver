package org.olmo.easjs.backend.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.event.MessageCountEvent;
import javax.mail.event.MessageCountListener;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.ReceivedDateTerm;

import org.olmo.easjs.backend._IObjectGetter;
import org.olmo.easjs.backend._IObjectSetter;
import org.olmo.easjs.server.object.SyncAttachment;
import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncMail;
import org.olmo.easjs.server.object.SyncObject;

import com.sun.mail.imap.IMAPBodyPart;
import com.sun.mail.imap.IMAPFolder;


public class IMAPManager extends IMAPSession implements _IObjectGetter, _IObjectSetter {
	Hashtable<String, Vector<SyncObject>> newMsgs=new Hashtable<String, Vector<SyncObject>>();
	public static final String BOUNDARY="\"RumpeltinskyyksnitlepmuR\"";

	public IMAPManager(
			String imapUsr, String imapPwd, String imapSrv, String imapProt,
			String smtpUsr, String smtpPwd, String smtpSrv, String smtpProt, 
			String smtpPort, boolean smtpTLS, boolean smtpAuth){
		super(true,imapUsr,imapPwd,imapSrv,imapProt,smtpUsr,smtpPwd,
				smtpSrv,smtpProt,smtpPort,smtpTLS,smtpAuth);
	}

	
	public SyncObject folderStat(String fid) {
		SyncObject sf = new SyncFolder();
		Vector<String> hier = new Vector<String>();
		StringTokenizer st = new StringTokenizer(fid, ".");
		while (st.hasMoreElements())
			hier.add(st.nextToken());
		sf.id = fid;
		if (fid.equalsIgnoreCase("inbox")) {
			sf.parentid = "0";
			sf.displayname = "Inbox";
			sf.type = "" + FOLDER_TYPE_INBOX;
		} else if (fid.equalsIgnoreCase("drafts")) {
			sf.parentid = "0";
			sf.displayname = "Drafts";
			sf.type = "" + FOLDER_TYPE_DRAFTS;
		} else if (fid.equalsIgnoreCase("trash")) {
			sf.parentid = "0";
			sf.displayname = "Trash";
			sf.type = "" + FOLDER_TYPE_WASTEBASKET;
		} else if (fid.equalsIgnoreCase("sent")
				|| fid.equalsIgnoreCase("sent items")) {
			sf.parentid = "0";
			sf.displayname = "Sent";
			sf.type = "" + FOLDER_TYPE_SENTMAIL;
		} else if (fid.equalsIgnoreCase("inbox.drafts")) {
			sf.parentid = hier.get(0);
			sf.displayname = "Drafts";
			sf.type = "" + FOLDER_TYPE_DRAFTS;
		} else if (fid.equalsIgnoreCase("inbox.trash")) {
			sf.parentid = hier.get(0);
			sf.displayname = "Trash";
			sf.type = "" + FOLDER_TYPE_WASTEBASKET;
		} else if (fid.equalsIgnoreCase("inbox.sent")) {
			sf.parentid = hier.get(0);
			sf.displayname = "Sent";
			sf.type = "" + FOLDER_TYPE_SENTMAIL;
		} else {
			//Match xlisted folders?
			if(folderTypes.get(fid.replace(".", "/"))!=null){
				SyncObject sf2=folderStat(folderTypes.get(fid.replace(".", "/")));
				sf.type=sf2.type;
			}
			if (hier.size() > 1) {
				sf.displayname = hier.lastElement();
				sf.parentid = fid.substring(0, fid.length() - 1
						- sf.displayname.length());
			} else {
				sf.parentid = "0";
				sf.displayname = fid;
			}
		}
		sf.mod = hier.lastElement();
		return sf;
	}

	
	public synchronized SyncObject objectStat(String fid, String id) {
		try {
			IMAPFolder folder=open(fid);
			Message m=folder.getMessageByUID(Integer.parseInt(id));
			return fromMessageToSyncObject(folder, m);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public Vector<SyncObject> folderList() {
		//Listamos los folder de correo
		Vector<SyncObject> ret = new Vector<SyncObject>();
		Vector<IMAPFolder> folders = new Vector<IMAPFolder>();
		folders.add(root);
		for(int i=0;i<folders.size();i++){
			IMAPFolder f=folders.get(i);
			SyncObject c=new SyncObject();
			c.id = f.getFullName();
			try {
				c.id = c.id.replace(f.getSeparator(), '.');
				IMAPFolder parent=(IMAPFolder)f.getParent();
				c.parentid = parent == null ? null : parent.getFullName();
				c.mod = f.getName();
				ret.add(c);
				if ((f.getType() & IMAPFolder.HOLDS_FOLDERS) != 0) {
					Folder[] childs = f.list();
					for(int j=0;j<childs.length;j++)
						folders.add((IMAPFolder)childs[j]);
				}
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
		}
		return ret;
	}

	
	public Vector<SyncObject> objectList(String fid, int cod) {
		Vector<SyncObject> ret=new Vector<SyncObject>();
		try {
			IMAPFolder folder=open(fid);
			ReceivedDateTerm dt = new ReceivedDateTerm(ComparisonTerm.GE, new Date(cod));
			Message found[] = folder.search(dt);
			for (int i = 0; i < found.length; i++) {
				Message m = found[i];
				if (m.isExpunged())
					continue;
				ret.add(fromMessageToSyncObject(folder, m));
			}
			return ret;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private SyncObject fromMessageToSyncObject(IMAPFolder folder, Message m){
		SyncObject c = new SyncObject();
		try {
			c.mod = ""+m.getSentDate().getTime();
			c.id = "" + folder.getUID(m);
			c.flags = m.isSet(Flags.Flag.SEEN) ? "1" : "0";
			c.parentid = folder.getFullName().replace(folder.getSeparator(), '.');
			return c;
		} catch (MessagingException e) {
			e.printStackTrace();
			return null;
		}
	}

	
	public SyncFolder folderGet(String fid) {
		return (SyncFolder)folderStat(fid);
	}

	
	public SyncObject objectGet(String fid, String id, int truncsize) {
		try {
			IMAPFolder f=open(fid);
			//Obtenemos msg del store
			MimeMessage msg = (MimeMessage)f.getMessageByUID(Long.parseLong(id));
			//Retornaremos mail
			SyncMail mail = new SyncMail(null);
			mail.parentid = fid;
			mail.id = id;
			fromMessageToSyncMail(msg, mail, truncsize);
			return mail;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private void fromMessageToSyncMail( MimeMessage msg, SyncMail mail, int trunc) throws IOException,
			MessagingException {
		//Contenido del correo
		SyncObject c=objectStat(mail.parentid, mail.id);
		if (c.flags != null){
			mail.read = c.flags;
			mail.flags= c.flags;
		}else{
			mail.read = "0";
			mail.flags= "0";
		}
		Object cnt = msg.getContent();
		String vtype=msg.getContentType();
		String contenido=null;
		boolean mime=false;
		if(vtype.toLowerCase().contains("text/html"))
			mime=true;
		if (cnt instanceof String) {
			if(mime){
				contenido="Content-Type: text/html;\n";
				contenido+=(String) cnt;
			}else{
				contenido = (String) cnt;
			}
		} else if (cnt instanceof Multipart) {
			contenido = getBody((Multipart) cnt);
			mime=true; 
		}
		if (contenido.length() > trunc) {
			contenido = contenido.substring(0, trunc);
			mail.bodytruncated = true;
		} else {
			mail.bodytruncated = false;
		}

		mail.body = contenido.replace("\r", "");
		mail.body = mail.body.replace("\n", "\r\n");
		mail.body = mail.body.trim();
		mail.bodysize = mail.body.length();

		if(mime){
			mail.mimedata=mail.body;
			mail.body=null;
			mail.mimesize=mail.bodysize;
			mail.mimetruncated=mail.bodytruncated ? "1" : "0";
		}

		mail.datereceived = msg.getReceivedDate();

		mail.to = adrToString(msg.getRecipients(RecipientType.TO));
		// ret.importance=Header "x-priority"
		mail.messageclass = "IPM.Note";
		mail.subject = msg.getSubject();
		mail.displayto = mail.to;
		mail.cc = adrToString(msg.getRecipients(RecipientType.CC));
		mail.from = adrToString(msg.getFrom());
		mail.reply_to = adrToString(msg.getReplyTo());

		// TODO Ojo, he quitado reply-to
		mail.reply_to = null;
		
		//Dejamos el false seen como estaba
		msg.setFlag(Flags.Flag.SEEN, mail.flags.equals("1") ? true : false);

		if (cnt instanceof Multipart) {
			Multipart mp = (Multipart) cnt;
			try {
				for (int i = 0; i < mp.getCount(); i++) {
					Part p = mp.getBodyPart(i);
					String dis = p.getDisposition();
					String fna = p.getFileName();
					if (dis != null && fna != null
							&& dis.equalsIgnoreCase(Part.ATTACHMENT)) {
						if (mail.attachments == null)
							mail.attachments = new Vector<SyncObject>();
						SyncAttachment at = new SyncAttachment();
						at.attsize = p.getSize();
						at.attname = mail.parentid + ":" + mail.id + ":" + fna;
						at.displayname = fna;
						at.attmethod = 1;
						for (Enumeration<Header> enu = p.getAllHeaders(); enu.hasMoreElements();) {
							Header h = enu.nextElement();
							if (h.getName().equals("content-id"))
								at.attoid = h.getValue();
						}
						mail.attachments.add(at);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				mail=null;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private String getBody(Multipart mPart) {
		//TODO me parece que hay que mandar todas las partes...
		// Loop through all of the BodyPart's
		try {
			StringBuffer sb=new StringBuffer();
			for (int i = 0; i < mPart.getCount(); i++) {
				// Grab the body part
				BodyPart bp = mPart.getBodyPart(i);
				// Grab the disposition for attachments
				String disposition = bp.getDisposition();
				// It's not an attachment
				if ((disposition == null || 
						!disposition.equalsIgnoreCase(Part.ATTACHMENT)) && 
							(bp instanceof MimeBodyPart || 
							 bp instanceof IMAPBodyPart)) {
					MimeBodyPart mbp = (MimeBodyPart) bp;

					// Check to see if we're in the screwy situation where
					// the message text is buried in another Multipart
					if (mbp.getContent() instanceof Multipart) {
						// Use recursion to parse the sub-Multipart
						return getBody((Multipart) mbp.getContent());
					} else {
						for(Enumeration<String> lines=mbp.getAllHeaderLines();lines.hasMoreElements();){
							String line=lines.nextElement();
							if(line.toLowerCase().startsWith("content-type") &&
									line.toLowerCase().indexOf("boundary")<0){
								if(!line.trim().endsWith(";")){
									line+=";";
								}
								line+="boundary="+BOUNDARY;
							}
							sb.append(line+"\n");
						}
						sb.append((String)mbp.getContent());
					}
				}
				return sb.toString();
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String adrToString(Address dirs[]){
		if(dirs==null)
			return null;
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<dirs.length;i++){
			if(i>0)
				sb.append(",").append(dirs[i].toString());
			else
				sb.append(dirs[i].toString());
		}
		return sb.toString();
	}

	
	public byte[] getAttachment(String attname) {
		// attname is folderid:id:part
		StringTokenizer st = new StringTokenizer(attname, ":");
		String fid = st.nextToken();
		String id = st.nextToken();
		String fname = st.nextToken();

		try {
			IMAPFolder folder = open(fid);
			Message found = folder.getMessageByUID(Integer.parseInt(id));
			Object cnt = found.getContent();
			if (cnt instanceof Multipart) {
				Multipart mp = (Multipart) cnt;
				for (int i = 0; i < mp.getCount(); i++) {
					Part p = mp.getBodyPart(i);
					String dis = p.getDisposition();
					String fna = p.getFileName();
					if (dis != null && 
							fna != null && 
							dis.equalsIgnoreCase(Part.ATTACHMENT) && 
							fna.equals(fname)) {
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						p.writeTo(baos);
						return baos.toByteArray();
					}
				}
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public boolean monitorFolder(String fid) {
		try{
			IMAPFolder f=open(fid);
			CustomMCL cmcl=new CustomMCL(f,fid);
			cmcl.start();
			f.addMessageCountListener(cmcl);
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	class CustomMCL extends Thread implements MessageCountListener {
		String fid;
		IMAPFolder f;
		public CustomMCL(IMAPFolder f, String fid){
			this.fid=fid;
			this.f=f;
		}
		
		public void messagesAdded(MessageCountEvent e) {
			Message msgs[]=e.getMessages();
			Vector<SyncObject> so=new Vector<SyncObject>();
			for(int i=0;i<msgs.length;i++){
				SyncObject c=fromMessageToSyncObject(f, msgs[i]);
				c.type="change";
				so.add(c);
			}
			newMsgs.put(fid, so);
		}
		
		public void messagesRemoved(MessageCountEvent e) {
			Message msgs[]=e.getMessages();
			Vector<SyncObject> so=new Vector<SyncObject>();
			for(int i=0;i<msgs.length;i++){
				SyncObject c=fromMessageToSyncObject(f, msgs[i]);
				c.type="delete";
				so.add(c);
			}
			newMsgs.put(fid, so);
		}
		
		public void run() {
			while(true)
				try {
					f.idle();
				} catch (MessagingException e) {
					e.printStackTrace();
				}
		}
	}

	
	public Vector<SyncObject> getMonitorizedChanges(String fid) {
		Vector<SyncObject> ret=newMsgs.get(fid);
		if(ret!=null){
			newMsgs.remove(fid);
			return ret;
		}
		return new Vector<SyncObject>() ;
	}
	
	public SyncObject folderChange(String parent, String oldid, String newid) {
		if(oldid.equals(FOLDER_TYPE_APPOINTMENT_ID) || 
				oldid.equals(FOLDER_TYPE_CONTACT_ID))
			return null; //No se puede cambiar esos Folders
		// Open parent folder
		try {
			if (oldid != null) {
				IMAPFolder old = open(oldid);
				IMAPFolder nuevo = open(newid);
				old.renameTo(nuevo);
			} else {
				IMAPFolder nuevo = open(newid);
				nuevo.create(IMAPFolder.HOLDS_FOLDERS | IMAPFolder.HOLDS_MESSAGES);
			}
			return folderStat(parent + "." + newid);
		} catch (MessagingException e) {
			e.printStackTrace();
			return null;
		}
	}

	
	public boolean folderDelete(String fid) {
		return false; //No permito borrado de folders.
	}

	
	public SyncObject objectChange(String fid, String id, SyncObject nuevo) {
		return nuevo; //Mail no puede cambiarse
	}

	
	public boolean objectDelete(String fid, String id) {
		try {
			IMAPFolder f = open(fid);
			Message m = f.getMessageByUID(Integer.parseInt(id));
			m.setFlag(Flags.Flag.DELETED, true);
			f.expunge();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	
	public boolean objectMove(String fid, String id, String newFid) {
		return false; //No se permite move en mails
	}

	
	public boolean sendMail(String rfc822, boolean forward, boolean reply, String parent) {
		try {
			ByteArrayInputStream bais=new ByteArrayInputStream(rfc822.getBytes());
			MimeMessage msg=new MimeMessage(session,bais);
			msg.setFrom(new InternetAddress(imapUser));
			Transport t=session.getTransport("smtps");
			t.connect("smtp.gmail.com",smtpUser,smtpPassword);
			t.sendMessage(msg, msg.getAllRecipients());
			t.close();
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}

	
	public boolean setReadFlag(String fid, String id, String flags) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID)||
				fid.equals(FOLDER_TYPE_CONTACT_ID))
			return true;
		try {
			IMAPFolder f = open(fid);
			Message m = f.getMessageByUID(Integer.parseInt(id));
			m.setFlag(Flags.Flag.SEEN, flags.equals("1") ? true : false);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}



}
