package org.olmo.easjs.backend.impl.google;

import java.util.HashSet;
import java.util.Vector;

import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.backend.DiffState;
import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncObject;

public class GoogleBackend extends Backend {
	ContactManager contactMgr;
	MailManager mailMgr;
	CalendarManager calendarMgr;
	String deviceId,protocolVersion;
	HashSet<String> monitorFolders=new HashSet<String>();
	
	@Override
	public boolean logoff() {
		mailMgr.close();
		return true;
	}

	@Override
	public boolean logon(String dom, String u, String pwd) {
		int ndx=u.indexOf("@gmail.com");
		if(ndx==-1){
			u+="@gmail.com";
		}
		try {
			_user=u;
			contactMgr=new ContactManager(u,pwd);
			mailMgr=new MailManager(u,pwd);
			calendarMgr=new CalendarManager(this,u,pwd);
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean Setup(String us, String devid, String protocolv) {
		deviceId=devid;
		protocolVersion=protocolv;
		return true;
	}

	@Override
	public SyncObject folderChange(String fid, String id, String newid, String type) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID))
			return calendarMgr.folderChange(fid, id, newid);
		if(fid.equals(FOLDER_TYPE_CONTACT_ID))
			return contactMgr.folderChange(fid, id, newid);
		return mailMgr.folderChange(fid, id, newid);
	}

	@Override
	public boolean folderDelete(String fid, String parent) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID))
			return calendarMgr.folderDelete(fid);
		if(fid.equals(FOLDER_TYPE_CONTACT_ID))
			return contactMgr.folderDelete(fid);
		return mailMgr.folderDelete(fid);
	}

	@Override
	public SyncFolder folderGet(String fid) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID))
			return calendarMgr.folderGet(fid);
		if(fid.equals(FOLDER_TYPE_CONTACT_ID))
			return contactMgr.folderGet(fid);
		return mailMgr.folderGet(fid);
	}

	@Override
	public Vector<SyncObject> folderList() {
		Vector<SyncObject> ret=new Vector<SyncObject>();
		ret.addAll(contactMgr.folderList());
		ret.addAll(calendarMgr.folderList());
		ret.addAll(mailMgr.folderList());
		return ret;
	}

	@Override
	public SyncObject folderStat(String fid) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID))
			return calendarMgr.folderStat(fid);
		if(fid.equals(FOLDER_TYPE_CONTACT_ID))
			return contactMgr.folderStat(fid);
		return mailMgr.folderStat(fid);
	}

	@Override
	public byte[] getAttachment(String attname) {
		return mailMgr.getAttachment(attname);
	}

	@Override
	public int getDeviceRWStatus(String user, String pwd, String devid) {
		return 0; // No soportamos el Wipe de dispositivos por ahora...
	}

	@Override
	public Vector<SyncObject> getSearchResults(String searchQuery) {
		return null; //Tampoco permitimos b�squedas
	}

	@Override
	public String getWasteBasket() {
		return null; //Todos los deletes son reales (no moves al WasteBasket)
	}

	@Override
	public SyncObject objectChange(String fid, String id, SyncObject nuevo) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID))
			return calendarMgr.objectChange(fid, id, nuevo);
		if(fid.equals(FOLDER_TYPE_CONTACT_ID))
			return contactMgr.objectChange(fid,id,nuevo);
		return mailMgr.objectChange(fid,id,nuevo);
	}

	@Override
	public boolean objectDelete(String fid, String id) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID))
			return calendarMgr.objectDelete(fid, id);
		if(fid.equals(FOLDER_TYPE_CONTACT_ID))
			return contactMgr.objectDelete(fid,id);
		return mailMgr.objectDelete(fid,id);
	}

	@Override
	public SyncObject objectGet(String fid, String id, int truncsize, boolean mimesupport) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID))
			return calendarMgr.objectGet(fid, id, truncsize);
		if(fid.equals(FOLDER_TYPE_CONTACT_ID))
			return contactMgr.objectGet(fid,id,truncsize);
		return mailMgr.objectGet(fid,id,truncsize);
	}

	@Override
	public Vector<SyncObject> objectList(String fid, int cutoffdate) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID))
			return calendarMgr.objectList(fid, cutoffdate);
		if(fid.equals(FOLDER_TYPE_CONTACT_ID))
			return contactMgr.objectList(fid,cutoffdate);
		return mailMgr.objectList(fid,cutoffdate);
	}

	@Override
	public boolean objectMoveTo(String fid, String id) {
		return false; // No hay moves
	}

	@Override
	public SyncObject objectStat(String fid, String id) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID))
			return calendarMgr.objectStat(fid, id);
		if(fid.equals(FOLDER_TYPE_CONTACT_ID))
			return contactMgr.objectStat(fid,id);
		return mailMgr.objectStat(fid,id);
	}

	@Override
	public boolean sendMail(String rfc822, boolean forward, boolean reply,
			String parent) {
		return mailMgr.sendMail(rfc822, forward, reply, parent);
	}

	@Override
	public boolean setReadFlag(String fid, String id, String flags) {
		return mailMgr.setReadFlag(fid, id, flags);
	}

	@Override
	public boolean alterPing() {
		return true;
	}

	@Override
	public Vector<SyncObject> alterPingChanges(String fid, Vector<SyncObject> syncstate, int cutoffdate) {
		if(monitorFolders.contains(fid)){
			return mailMgr.getMonitorizedChanges(fid);
		}else{
			//Procedimiento normal
			Vector<SyncObject> msglist = objectList(fid, cutoffdate);
			if (msglist == null)
				return new Vector<SyncObject>();;
			return DiffState.GetDiff(syncstate, msglist);
		}
	}

	@Override
	public boolean wantMonitorFolder(String fid) {
		if(fid.equals(FOLDER_TYPE_APPOINTMENT_ID)||
				fid.equals(FOLDER_TYPE_CONTACT_ID))
			return false;
		if(mailMgr.monitorFolder(fid)){
			monitorFolders.add(fid);
			return true;
		}
		return false;
	}

}
