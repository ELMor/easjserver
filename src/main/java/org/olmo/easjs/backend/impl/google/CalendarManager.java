package org.olmo.easjs.backend.impl.google;

import java.io.StringReader;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.PropertyList;

import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.backend._IObjectGetter;
import org.olmo.easjs.backend._IObjectSetter;
import org.olmo.easjs.server.object.SyncAppointment;
import org.olmo.easjs.server.object.SyncAttendee;
import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncObject;

import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.Category;
import com.google.gdata.data.DateTime;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.calendar.CalendarEventEntry;
import com.google.gdata.data.calendar.CalendarEventFeed;
import com.google.gdata.data.calendar.EventWho;
import com.google.gdata.data.extensions.When;
import com.google.gdata.data.extensions.Where;
import com.google.gdata.util.RedirectRequiredException;
import com.google.gdata.util.ResourceNotFoundException;

public class CalendarManager implements _IObjectGetter, _IObjectSetter {
	CalendarService service=null;
	String srvUrl,user,password,eventUrl;
	private SyncFolder calendarFolder=new SyncFolder();
	Backend backend;
	
	public CalendarManager(Backend bend, String usr, String pass) throws Exception {
		calendarFolder.displayname="Agenda";
		calendarFolder.id=FOLDER_TYPE_APPOINTMENT_ID;
		calendarFolder.mod="NOMOD";
		calendarFolder.type=""+FOLDER_TYPE_APPOINTMENT;
		calendarFolder.parentid="0";
		calendarFolder.read="1";
		user=usr;
		password=pass;
		srvUrl="http://www.google.com/calendar/feeds/"+user+"/private/full";
		eventUrl="http://www.google.com/calendar/feeds/"+user+"/events/";
		service=new CalendarService("EASjServer");
		service.setUserCredentials(user,password);
		backend=bend;
	}
	
	
	public SyncFolder folderGet(String fid) {
		if(fid.equals(calendarFolder.id))
			return calendarFolder;
		return null;
	}

	
	public byte[] getAttachment(String attname) {
		return null;
	}

	
	public Vector<SyncObject> getMonitorizedChanges(String fid) {
		// No se como hacerlo con los contactos
		return null;
	}

	
	public boolean monitorFolder(String fid) {
		// No se como hacerlo con los Calendarios
		return false;
	}

	
	public SyncObject objectStat(String fid, String srvId) {
		String bendId=backend.getBendIdFromSrvId(srvId);
		try {
			URL u=new URL(bendId);
			CalendarEventEntry ce=service.getEntry(u, CalendarEventEntry.class);
			return fromCalendarEventEntryToStat(ce);
		}catch(ResourceNotFoundException rnfe){
			return null;
		}catch(Exception e){
			System.out.println("For srvId:"+srvId+"<->bendId:"+bendId);
			e.printStackTrace();
		}
		return null;
	}

	
	public SyncObject objectGet(String fid, String srvId, int truncSize) {
		String bendId=backend.getBendIdFromSrvId(srvId);
		try {
			URL u=new URL(bendId);
			CalendarEventEntry ce=service.getEntry(u, CalendarEventEntry.class);
			SyncAppointment appo=new SyncAppointment(null);
			appo.id=srvId;
			appo.mod=ce.getEtag();
			fromCalendarEntryToSyncAppointment(ce,appo);
			return appo;
		}catch(ResourceNotFoundException rnfe){
			return null;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	
	public Vector<SyncObject> folderList() {
		Vector<SyncObject> ret=new Vector<SyncObject>();
		ret.add(calendarFolder);
		return ret;
	}

	
	public SyncObject folderStat(String fid) {
		return folderGet(fid);
	}

	
	public Vector<SyncObject> objectList(String fid, int cutOffDate) {
		Vector<SyncObject> ret = new Vector<SyncObject>();
		try{
			URL qu=new URL(srvUrl);
			CalendarEventFeed cf=null;
			try {
				cf=service.getFeed(qu, CalendarEventFeed.class);
			}catch(RedirectRequiredException re){
				qu=new URL(re.getRedirectLocation());
				cf=service.getFeed(qu, CalendarEventFeed.class);
			}
			List<CalendarEventEntry> ces=cf.getEntries();
			for(int i=0;i<ces.size();i++){
				CalendarEventEntry ce=ces.get(i);
				String srvId=backend.getSrvIdFromBendId(ce.getSelfLink().getHref());
				SyncObject add=objectStat(fid, srvId);
				if(add!=null)
					ret.add(add);
			}
			return ret;
		}catch(Exception e){
			e.printStackTrace();
			ret=null;
		}
		return null;
	}

	private SyncObject fromCalendarEventEntryToStat(CalendarEventEntry ce) {
		if(ce!=null){
			SyncObject c=new SyncObject();
			c.id=backend.getSrvIdFromBendId(ce.getSelfLink().getHref());
			c.mod=ce.getEtag();
			c.flags="0";
			c.read="0";
			return c;
		}
		return null;
	}

	
	public SyncObject folderChange(String parent, String oldid, String newid) {
		// No permitido
		return null;
	}

	
	public boolean folderDelete(String fid) {
		// No permitido
		return false;
	}

	
	public SyncObject objectChange(String fid, String srvId, SyncObject nuevo) {
		try {
			SyncAppointment appo=(SyncAppointment)nuevo;
			CalendarEventEntry ce=null;
			if(srvId==null){ //Crear. srvId es ClientId
				ce=new CalendarEventEntry();
				fromSyncAppointmentToCalendarEventEntry(appo,ce);
				URL u=new URL(srvUrl);
				ce=service.insert(u, ce);
				srvId=backend.getSrvIdFromBendId(ce.getSelfLink().getHref());
			}else{ //Modificar
				String bendId=backend.getSrvIdFromBendId(srvId);
				URL qu=new URL(bendId);
				ce=service.getEntry(qu, CalendarEventEntry.class);
				fromSyncAppointmentToCalendarEventEntry((SyncAppointment)nuevo, ce);
				URL eu=new URL(ce.getEditLink().getHref());
				ce=service.update(eu, ce);
			}
			return objectStat(fid, srvId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	private void fromSyncAppointmentToCalendarEventEntry(SyncAppointment appo,
			CalendarEventEntry ce) {
		
		ce.setTitle(new PlainTextConstruct(appo.subject));
		ce.setIcalUID(appo.uid);
		When cuando=new When();
		cuando.setStartTime(new DateTime(appo.starttime.getTime()));
		cuando.setEndTime(new DateTime(appo.endtime.getTime()));
		if(appo.categories!=null){
			for(int i=0;i<appo.categories.size();i++){
				
			}
		}
	
	}

	private void fromCalendarEntryToSyncAppointment(CalendarEventEntry ce,
			SyncAppointment appo) {
		
		appo.subject=ce.getTitle().getPlainText();
		Set<Category> scs=ce.getCategories();
		if(scs!=null){
			appo.categories=new Vector<String>();
			for(Iterator<Category> i=scs.iterator();i.hasNext();){
				Category cat=i.next();
				if(!(cat.getLabel()==null))
					appo.categories.add(i.next().getLabel());
			}
			if(appo.categories.size()==0)
				appo.categories=null;
		}
		
		List<Where> llw=ce.getLocations();
		if(llw!=null){
			appo.location="";
			for(int i=0;i<llw.size();i++){
				String toadd=llw.get(i).getLabel();
				if(toadd!=null)
					appo.location+=toadd+";";
			}
			if(appo.location.length()==0)
				appo.location=null;
		}

		List<EventWho> lpes=ce.getParticipants();
		if(lpes!=null){
			appo.attendees=new Vector<SyncAttendee>();
			for(int i=0;i<lpes.size();i++){
				SyncAttendee sa=new SyncAttendee(null);
				EventWho ew=lpes.get(i);
				sa.email=ew.getEmail();
				sa.name=ew.getValueString();
				if(sa.email!=null ||sa.name!=null)
					appo.attendees.add(sa);
			}
			if(appo.attendees.size()==0)
				appo.attendees=null;
		}
		if(ce.getTimes()!=null && ce.getTimes().size()>0){
			When cuando=ce.getTimes().get(0);
			if(cuando!=null){
				long st=cuando.getStartTime().getValue();
				long et=cuando.getEndTime().getValue();
				appo.starttime=new Date(st);
				appo.endtime=new Date(et);
			}
		}
		//TODO appo.uid=toHEXString(ce.getIcalUID());
		
		appo.dtstamp=new Date(ce.getPublished().getValue());
		
		//Intento de recurrences.
		if(ce.getRecurrence()!=null){
			String mcs=ce.getRecurrence().getValue();
			if(mcs!=null){
				try {
					mcs="BEGIN:VCALENDAR\n"+mcs+"\nEND:VCALENDAR\n";
					StringReader srmcs=new StringReader(mcs);
					CalendarBuilder cb=new CalendarBuilder();
					Calendar cal=cb.build(srmcs);
					PropertyList pl=cal.getProperties();
					ComponentList cl=cal.getComponents();
					System.out.println(pl.size());
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		if(ce.getSummary()!=null)
			appo.body=ce.getSummary().getPlainText();
	}

	private String toHEXString(String s){
		if(s==null)
			return null;
		String ret="";
		for(int i=0;i<s.length();i++){
			String k=String.format("%H",(byte)s.charAt(i));
			while(k.length()<2)
				k="0"+k;
			ret+=k;
		}
		return ret;
	}
	
	
	public boolean objectDelete(String fid, String srvId) {
		try {
			URL qu=new URL(backend.getBendIdFromSrvId(srvId));
			CalendarEventEntry ce=service.getEntry(qu, CalendarEventEntry.class);
			ce.delete();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	
	public boolean objectMove(String fid, String id, String newFid) {
		// No permitido (Donde se moveria?)
		return false;
	}

	
	public boolean sendMail(String rfc822, boolean forward, boolean reply,
			String parent) {
		// Sin sentido
		return false;
	}

	
	public boolean setReadFlag(String fid, String id, String flags) {
		// Sin sentido
		return false;
	}

}
