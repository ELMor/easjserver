package org.olmo.easjs.backend;

import java.util.Collections;
import java.util.Vector;

import org.olmo.easjs.Cons;
import org.olmo.easjs.server.object.SyncObject;
import org.olmo.easjs.wbxml.eas.cp.CPAirSync;

public class DiffState implements Cons{
	Backend backend;
	Vector<SyncObject> syncstate=new Vector<SyncObject>();
	Vector<SyncObject> changes = new Vector<SyncObject>();
	int flags;
	
	public DiffState(Backend b){
		backend=b;
	}
	public static Vector<SyncObject> GetDiff(Vector<SyncObject> viejo, Vector<SyncObject> nuevo){
		Vector<SyncObject> ret=new Vector<SyncObject>();
		
		Collections.sort(viejo);
		Collections.sort(nuevo);
		
		int iold=0;
		int inew=0;
		
		SyncObject change=new SyncObject();
		while(true){
			if(iold>=viejo.size() || inew>=nuevo.size())
				break;
			SyncObject iviejo=viejo.get(iold);
			SyncObject inuevo=nuevo.get(inew);
			
			if(iviejo.id.equals(inuevo.id)){
				//Both messages availables, compare flags and mod
				if(iviejo.flags!=null && inuevo.flags!=null && !iviejo.flags.equals(inuevo.flags)){
					//Flags changed
					change.type="flags";
					change.id=inuevo.id;
					change.flags=inuevo.flags;
					ret.add(change);
					change=new SyncObject();
				}
				if(!iviejo.mod.equals(inuevo.mod)){
					change.type="change";
					change.id=inuevo.id;
					ret.add(change);
					change=new SyncObject();
				}
				inew++;
				iold++;
			} else {
				if(iviejo.id.compareTo(inuevo.id)<0){
					//Message in new seems to ha disappeared (delete)
					change.type="delete";
					change.id=iviejo.id;
					ret.add(change);
					change=new SyncObject();
					iold++;
				} else {
					//Message in new seems to be new
					change.type="change";
					change.flags=CPAirSync.NewMessage;
					change.id=inuevo.id;
					ret.add(change);
					change=new SyncObject();
					inew++;
				}
			}
		}
		while(iold<viejo.size()){
			//All data left in viejo has been deleted
			SyncObject iviejo=viejo.get(iold);
			change.type="delete";
			change.id=iviejo.id;
			ret.add(change);
			change=new SyncObject();
			iold++;
		}
		while(inew<nuevo.size()){
			//All data left in nuevo has been added
			SyncObject inuevo=nuevo.get(inew);
			change.type="change";
			change.flags=CPAirSync.NewMessage;
			change.id=inuevo.id;
			ret.add(change);
			change=new SyncObject();
			inew++;
		}
		return ret;
	}
	
	public void updateState(String type, SyncObject change){
		if(type.equals("change")){
			for(int i=0;i<syncstate.size();i++){
				SyncObject ins=syncstate.get(i);
				if(change.id.equals(ins.id)){
					syncstate.set(i, change);
					return;
				}
			}
			//Not found: add
			syncstate.add(change);
		}else{
			for(int i=0;i<syncstate.size();i++){
				SyncObject ins=syncstate.get(i);
				if(change.id.equals(ins.id)){
					if(type.equals("flags")){
						ins.flags=change.flags;
					} else if(type.equals("delete")){
						syncstate.remove(i);
					}
					return;
				}
			}
		}
	}

	public boolean isConflict(String type, String folderid, String id){
		SyncObject stat=backend.objectStat(folderid, id);
		if(stat==null){
			//Message is gone
			if(type.equals("change")){
				return true; //delete here, but changed there
			}else{
				return false; //All other remote changes still result in a delete (no conflict)
			}
		}
		
		SyncObject oldstat=null,state=null;
		if(syncstate!=null)
			for(SyncObject loop:syncstate){
				if(loop.id.equals(id)){
					oldstat=loop;
					state=loop;
					break;
				}
			}
		
		if(oldstat==null)
			return false;
		
		if(!oldstat.mod.equals(state.mod)){
			//Changed here
			if(type.equals("delete")||type.equals("change")){
				return true; //deleted there
			}else{
				return false;
			}
		}
		return false;
	}
	
	public Vector<SyncObject> getState(){
		return syncstate;
	}
	
	/**
	 * Calcula el numero de dias que corresponden al codigo restrict
	 * @param restrict
	 * @return
	 */
	public int getCutOffDate(int restrict) {
		int back = -1;
		switch (restrict) {
		case FILTERTYPE_1DAY:
			back = 60 * 60 * 24;
			break;
		case FILTERTYPE_3DAYS:
			back = 60 * 60 * 24 * 3;
			break;
		case FILTERTYPE_1WEEK:
			back = 60 * 60 * 24 * 7;
			break;
		case FILTERTYPE_2WEEKS:
			back = 60 * 60 * 24 * 14;
			break;
		case FILTERTYPE_1MONTH:
			back = 60 * 60 * 24 * 31;
			break;
		case FILTERTYPE_3MONTHS:
			back = 60 * 60 * 24 * 31 * 3;
			break;
		case FILTERTYPE_6MONTHS:
			back = 60 * 60 * 24 * 31 * 6;
			break;
		}
		if (back != -1) {
			return (int) (System.currentTimeMillis() / 1000 - back);
		} else {
			return 0;
		}
	}
	/**
	 * Calcula el tama�o de truncamiento dependiendo del codigo truncation
	 * @param truncation
	 * @return
	 */
	public int getTruncSize(int truncation) {
		switch (truncation) {
		case TRUNCATION_HEADERS:
			return 0;
		case TRUNCATION_512B:
			return 512;
		case TRUNCATION_1K:
			return 1024;
		case TRUNCATION_5K:
			return 5 * 1024;
		case TRUNCATION_SEVEN:
		case TRUNCATION_ALL:
			return 1024 * 1024 * 10;
		default:
			return 1024;
		}
	}

}
