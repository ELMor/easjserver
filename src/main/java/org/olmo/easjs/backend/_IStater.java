package org.olmo.easjs.backend;

import java.util.Vector;

import org.olmo.easjs.Cons;
import org.olmo.easjs.server.object.SyncObject;

public interface _IStater extends Cons {
	/**
	 * Recoje informaci�n basica del Folder
	 * @param fid Identificador del Folder
	 * @return Informacion b�sica (mod y flags)
	 */
	public SyncObject folderStat(String fid);

	/**
	 * Recoje informaci�n b�sica del objeto
	 * @param fid Folder al que pertenece el objeto
	 * @param id Identificador del Objeto
	 * @return Informacion b�sica (mod y flags)
	 */
	public SyncObject objectStat(String fid, String id);
	
	/**
	 * Lista de Folders
	 * @return Lista de Folders
	 */
	public Vector<SyncObject> folderList();
	
	/**
	 * Lista de Objetos de un Folder
	 * @param fid Identificador del Folder
	 * @param cutOffDate en millis, listar los objetos posteriores a
	 * @return
	 */
	public Vector<SyncObject> objectList(String fid, int cutOffDate);
}
