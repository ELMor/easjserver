package org.olmo.easjs.backend;

import java.util.Vector;

import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncObject;

public class HierExport extends DiffState{

	HierImport importHier;
	
	/**
	 * Numero del paso en el procesamiento synchronize
	 */
	int step = 0;

	public HierExport(Backend b,HierImport imp, Vector<SyncObject> st, 
			int flg) {
		super(b);
		this.syncstate = st;
		this.flags = flg;
		this.importHier = imp;
		this.changes = new Vector<SyncObject>();
		Vector<SyncObject> folderlist = backend.folderList();
		if (folderlist.size() == 0)
			return;
		if (syncstate == null)
			syncstate = new Vector<SyncObject>();
		changes = GetDiff(syncstate, folderlist);
	}

	public int getChangeCount() {
		return changes != null ? changes.size() : 0;
	}

	/**
	 * Sincroniza Folders
	 * @return
	 */
	public boolean synchronize() {
		if (step < changes.size()) {
			SyncObject c = changes.get(step);
			if (!c.id.equals("")) {
				SyncFolder folder = backend.folderGet(c.id);
				if (c.type.equals("change")) {
					SyncObject stat = backend.folderStat(c.id);
					if (folder == null)
						return false;
					if (importHier.folderChange(folder) != null
							|| (flags & BACKEND_DISCARD_DATA) != 0) {
						updateState("change", stat);
					}
				} else if (c.type.equals("delete")) {
					if (importHier.folderDeletion(folder) != null
							|| (flags & BACKEND_DISCARD_DATA) != 0) {
						updateState("delete", c);
					}
				}
			}
			step++;
			return true;
		} else {
			return false;
		}
	}

}
