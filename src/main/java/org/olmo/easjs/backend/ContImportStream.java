package org.olmo.easjs.backend;

import java.util.Vector;

import org.olmo.easjs.server.object.SyncObject;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPAirSync;
import org.olmo.easjs.wbxml.eas.cp.CPEmail;

public class ContImportStream extends ContImport {
	String type;
	EASEncoder xo;
	Vector<SyncObject> seenObjects;
	
	public ContImportStream(Backend b, String folderid, EASEncoder enc, String t){
		super(b,folderid,new Vector<SyncObject>(),0);
		xo=enc;
		type=t;
		seenObjects=new Vector<SyncObject>(); 
	}
	
	@Override
	public SyncObject change(String id, SyncObject msg){
		if(seenObjects.contains(msg))
			return null;
		seenObjects.add(msg);
		if(msg.flags==null || msg.flags.equals(CPAirSync.NewMessage))
			xo.open(CPAirSync.Add);
		else
			xo.open(CPAirSync.Change);
		xo.element(CPAirSync.ServerEntryId,id);
		xo.element(CPAirSync.Data, msg);
		xo.close();
		return msg;
	}
	
	@Override
	public boolean delete(String id){
		xo.open(CPAirSync.Delete);
		xo.element(CPAirSync.ServerEntryId,id);
		xo.close();
		return true;
	}
	
	@Override
	public boolean readFlag(String id, String flags){
		xo.open(CPAirSync.Change);
		xo.element(CPAirSync.ServerEntryId,id);
		xo.open(CPAirSync.Data);
		xo.element(CPEmail.Read,flags);
		xo.close();
		xo.close();
		return true;
	}
	@Override
	public String move(String id, String newFolder){
		return id;
	}
}
