package org.olmo.easjs.backend;

import java.util.Vector;

import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncObject;

public class HierImport extends DiffState {

	public HierImport(Backend b) {
		super(b);
	}

	String _user;
	
	public void Config(Vector<SyncObject> state){
		syncstate=state;
	}
	
	public String folderChange(SyncFolder folder){
		return folderChange(
				folder.id, 
				folder.parentid, 
				folder.displayname, 
				folder.type);
	}
	
	public String folderChange(String id, String parent, String displayname,String type){
		if(id!=null){
			SyncObject change=new SyncObject(
					id,null,"0",displayname,parent,null);
			updateState("change", change);
		}
		SyncObject it=backend.folderChange(parent, id, displayname, type);
		if(it!=null){
			updateState("change", it);
			return it.id;
		}
		
		return null;
	}
	
	public String folderDeletion(SyncFolder folder){
		return folderDeletion(folder.id,folder.parentid);
	}
	/**
	 * Debe retornar el estado del borrado (FolderCreate)
	 * @param id id del Folder a crear
	 * @param parent Padre del anterior
	 * @return Estado del borrado. Supongo 1=Ok, 2=Error
	 */
	public String folderDeletion(String id, String parent){
		SyncObject m=new SyncObject();
		m.id=id;
		updateState("delete", m);
		if(backend.folderDelete(id, parent))
			return "1";
		return "2";
	}
}
