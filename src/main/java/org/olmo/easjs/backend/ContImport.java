package org.olmo.easjs.backend;

import java.util.Vector;

import org.olmo.easjs.Cons;
import org.olmo.easjs.server.object.SyncObject;

public class ContImport extends DiffState {
	String fid;
	
	public ContImport(Backend b, String folderid,
			Vector<SyncObject> state, int flags) {
		super(b);
		fid=folderid;
		syncstate=state;
		this.flags=flags;
	}

	public SyncObject change(String id, SyncObject message){
		if(id!=null){
			boolean conflict=isConflict("change", fid, id);
			SyncObject change=new SyncObject();
			change.id=id;
			change.mod="0";
			change.parentid=fid;
			change.flags=message.read!=null?message.read:"0";
			updateState("change", change);
			if(conflict && flags==Cons.CONFLICT_OVERWRITE_PIM)
				return null;
		}
		SyncObject stat=backend.objectChange(fid, id, message);
		updateState("change", stat);
		return stat; 
	}
	
	public boolean delete(String id){
		boolean conflict=isConflict("delete", fid, id);
		SyncObject c=new SyncObject();
		c.id=id;
		updateState("delete", c);
		if(conflict && flags==Cons.CONFLICT_OVERWRITE_PIM)
			return true;
		backend.objectDelete(fid, id);
		return true;
	}
	
	public boolean readFlag(String id, String flags){
		SyncObject c=new SyncObject();
		c.id=id;
		c.flags=flags;
		updateState("flags", c);
		backend.setReadFlag(fid,id,flags);
		return true;
	}
	
	public String move(String id, String newFolder){
		return id; //No hay nada m�s
	}
}
