package org.olmo.easjs;

import java.util.Hashtable;

import org.olmo.common.MultiConfigProp;
import org.olmo.easjs.store.GenericStore;

public class Conf {
	public static boolean debug;
	public static boolean debugHttp;
	public static String debugDir;
	public static boolean debugXmlStreams;
	public static String keyStoreName;
	public static String keyStorePwd;
	public static String trustStoreName;
	public static String trustStorePwd;
	public static boolean looseProvision;
	public static String storeClass;
	public static String storeUri;
	public static String storeUser;
	public static String storePass;
	public static Hashtable<String, String> backends=new Hashtable<String, String>();
	public static String serverClass;
	public static boolean httpServer;
	public static boolean httpsServer;
	public static int maxThreads=5;
	public static GenericStore st=null;
	
	public static void load(MultiConfigProp mcp){
		debug=mcp.getBoolean("easjs.debug");
		debugHttp=mcp.getBoolean("easjs.debugHttp");
		debugDir=mcp.get("easjs.debug.dir");
		debugXmlStreams=mcp.getBoolean("easjs.debug.xmlstreams");
		keyStoreName=mcp.get("easjs.ks.name");
		keyStorePwd=mcp.get("easjs.ks.pass");
		trustStoreName=mcp.get("easjs.ts.name");
		trustStorePwd=mcp.get("easjs.ts.pass");
		looseProvision=mcp.getBoolean("easjs.provision.loose");
		mcp.addSelector("easjs.store");
		storeClass=mcp.get("easjs.store.driver");
		storeUri=mcp.get("easjs.store.uri");
		storeUser=mcp.get("easjs.store.user");
		storePass=mcp.get("easjs.store.pass");
		backends=mcp.get("easjs.domain", "Name", "BackendClassName");
		serverClass=mcp.get("easjs.webserver");
		httpServer=mcp.getBoolean("easjs.webserver.http");
		maxThreads=mcp.getInt("easjs.webserver.threads");
		httpsServer=mcp.getBoolean("easjs.webserver.https");
		//Setup storage
		try {
			st = (GenericStore)Class.forName(storeClass).newInstance();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		if(st!=null)
			st.login(storeUser,storePass,storeUri);
		else
			throw new RuntimeException("Cannot create Storage!");

	}
}
