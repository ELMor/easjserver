package org.olmo.easjs.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.olmo.common.Util;
import org.olmo.easjs.Conf;
import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.server.commands.CreateCollection;
import org.olmo.easjs.server.commands.DeleteCollection;
import org.olmo.easjs.server.commands.FolderCreate;
import org.olmo.easjs.server.commands.FolderDelete;
import org.olmo.easjs.server.commands.FolderSync;
import org.olmo.easjs.server.commands.FolderUpdate;
import org.olmo.easjs.server.commands.GetAttachment;
import org.olmo.easjs.server.commands.GetHierarchy;
import org.olmo.easjs.server.commands.GetItemEstimate;
import org.olmo.easjs.server.commands.MeetingResponse;
import org.olmo.easjs.server.commands.MoveCollection;
import org.olmo.easjs.server.commands.MoveItems;
import org.olmo.easjs.server.commands.Notify;
import org.olmo.easjs.server.commands.Ping;
import org.olmo.easjs.server.commands.Provision;
import org.olmo.easjs.server.commands.Search;
import org.olmo.easjs.server.commands.SendMail;
import org.olmo.easjs.server.commands.SmartForward;
import org.olmo.easjs.server.commands.SmartReply;
import org.olmo.easjs.server.commands.Sync;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class EASHandler implements HttpHandler,OLMOHttpHandler {

	static Hashtable<String, Backend> bends = new Hashtable<String, Backend>();

	String domain, user, password, deviceid;
	String protVers = "1.0";
	String policyKey = null;
	String userAgent = "unknown";
	Backend backend = null;
	String requestMethod;
	String command;
	Map<String, String> params;
	HashMap<String, String> reqHe;
	HashMap<String, String> resHe=new HashMap<String, String>();
	OutputStream os = null;
	InputStream is = null;
	HttpExchange httpXch=null;
	OLMOExchange xch=null;
	String authHeader;
	
	Logger log = Logger.getLogger("org.olmo.easjs.server.MSASHandler");

	
	public void handle(OLMOExchange x) throws IOException {
		xch=x;
		requestMethod=x.requestMethod;
		params=x.params;
		is=x.is;
		os=x.os;
		reqHe=x.reqHe;
		handle();
	}

	
	public void handle(HttpExchange x) throws IOException {
		httpXch=x;
		requestMethod = x.getRequestMethod();
		// Parseo de la parte query de la url (User, DeviceId y Devicetype
		params = Util.getQueryMap(x.getRequestURI().getQuery());
		is = x.getRequestBody();
		os = x.getResponseBody();
		Headers h1=x.getRequestHeaders();
		Set<String> keys=h1.keySet();
		for(Iterator<String> keyi=keys.iterator();keyi.hasNext();){
			String key=keyi.next();
			reqHe.put(key, h1.getFirst(key));
		}
		handle();
	}
	
	public InputStream getRequestBody(){
		return is;
	}
	
	public OutputStream getResponseBody(){
		return os;
	}
	
	public void addResponseHeader(String headName, String value){
		if(xch!=null){
			xch.addResponseHeader(headName, value);
		}else if(httpXch!=null){
			httpXch.getResponseHeaders().add(headName, value);
		}
	}
	
	private void addAuthResponseHeader(){
		addResponseHeader("WWW-Authenticate", "Basic realm=\"EASJS\"");
	}
	
	private void addMSSAS_VHeader(){
		addResponseHeader("MS-Server-ActiveSync", "6.5.7638.1");
	}
	
	private void addMSASP_VHeader(){
		addResponseHeader("MS-ASProtocolVersions", "1.0,2.0,2.1,2.5");
	}
	
	private void addMSASCommandsHeader(){
		addResponseHeader("MS-ASProtocolCommands",
				"Sync,SendMail,SmartForward,SmartReply,"
						+ "GetAttachment,GetHierarchy,"
						+ "CreateCollection,DeleteCollection,"
						+ "MoveCollection,FolderSync,FolderCreate,"
						+ "FolderDelete,FolderUpdate,MoveItems,"
						+ "GetItemEstimate,MeetingResponse,Provision,"
						+ "ResolveRecipients,ValidateCert,Search,Ping");
	}
	
	public void sendResponseHeaders(int code, int size) throws IOException{
		if(xch!=null)
			xch.sendResponseHeaders(code, size);
		else if(httpXch!=null)
			httpXch.sendResponseHeaders(code, size);
	}
	
	private void handle() throws IOException{

		protVers = sval(reqHe.get("Ms-Asprotocolversion"),	protVers);
		policyKey = sval(reqHe.get("X-Ms-PolicyKey"), policyKey);
		userAgent = sval(reqHe.get("User-Agent"), userAgent);

		//Conf.looseProvision;

		// Si se usa post, hay que comrpobar que estan los parametros
		if (requestMethod.equals("POST")) {
			if (params.get("User") == null || 
					params.get("DeviceId") == null || 
					params.get("DeviceType") == null) {
				sendResponseHeaders(401, 0);
				print("Your device requested the URL without the required GET parameters");
				os.close();
				log.info("Seems to be accessed by a browser...");
				return;
			}
		}
		// Tipo de Comando
		command = params.get("Cmd");
		if(command!=null)
			log.info("Command "+ command);
		// Envio autorizacion?
		String auth=reqHe.get("Authorization");
		byte authDecoded[] = (auth==null ? null : new Base64().decode(auth.substring(6)));
		if (auth==null || authDecoded == null) {
			// No lo hizo
			addAuthResponseHeader();
			sendResponseHeaders(401, 0);
			print("Access denied. Please send authorisation information");
			os.close();
			log.info("No auth info received.");
			return;
		}
		// Obtenemos user, password y realm (domain) de la autentificacion
		// basica.
		String aus = new String(authDecoded);
		if (aus.indexOf("\\") >= 0) {
			domain = aus.substring(0, aus.indexOf("\\"));
		} else {
			domain = "";
			aus = " " + aus;
		}
		user = aus.substring(domain.length() + 1, aus.indexOf(":"));
		password = aus.substring(domain.length() + user.length() + 2);
		// Obtenemos algunos parametros de los headers
		// Creamos/reusamos el backend
		deviceid = params.get("DeviceId");
		backend = bends.get(deviceid);
		if (backend == null) {
			String bcn = Conf.backends.get(domain);
			try {
				backend = (Backend) Class.forName(bcn).newInstance();
				// Hacemos logon en el backend
				if (!backend.logon(domain, user, password)) {
					addAuthResponseHeader();
					sendResponseHeaders(401, 0);
					print("Access denied: backend logon failed");
					os.close();
					return;
				}
				bends.put(deviceid, backend);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("Domain " + domain + " is unknown");
			}
		}
		// Permitimos al backend que haga un setup del dispositivo y version del
		// protocolo.
		if (!backend.Setup(user, deviceid, protVers)) {
			addAuthResponseHeader();
			sendResponseHeaders(401, 0);
			print("Access denied or user '" + user + "' unknown.");
			os.close();
			return;
		}
		/*
		 * TODO completar Provisioning //Chequeo del policy header
		 * if(!requestMethod.equals("OPTIONS") && !command.equals("Ping") &&
		 * !command.equals("Provision") && (!looseProvision||(looseProvision &&
		 * policyKey!=null))){ resHe.add("MS-Server-ActiveSync", "6.5.7638.1");
		 * resHe.add("MS-ASProtocolVersions","1.0,2.0,2.1,2.5");
		 * resHe.add("MS-ASProtocolCommands",
		 * "Sync,SendMail,SmartForward,SmartReply,GetAttachment,GetHierarchy,CreateCollection,DeleteCollection,MoveCollection,FolderSync,FolderCreate,FolderDelete,FolderUpdate,MoveItems,GetItemEstimate,MeetingResponse,Provision,ResolveRecipients,ValidateCert,Search,Ping"
		 * ); resHe.add("Cache-Control","private"); x.sendResponseHeaders(449,
		 * 0); os.close(); return; }
		 */
		// Al tajo. Que metodo quieres?
		if (requestMethod.equals("OPTIONS")) {
			addMSSAS_VHeader();
			addMSASP_VHeader();
			addMSASCommandsHeader();
			sendResponseHeaders(0, 0);
			os.close();
			return;
		} else if (requestMethod.equals("POST")) {
			addMSSAS_VHeader();
			log.info("POST cmd:" + command);
			try {
				CommandHandler ch = null;
				if (command.equals("Sync")) {
					ch = new Sync();
				} else if (command.equals("SendMail")) {
					ch = new SendMail();
				} else if (command.equals("SmartForward")) {
					ch = new SmartForward();
				} else if (command.equals("SmartReply")) {
					ch = new SmartReply();
				} else if (command.equals("GetAttachment")) {
					ch = new GetAttachment(params.get("AttachmentName"));
				} else if (command.equals("GetHierarchy")) {
					ch = new GetHierarchy();
				} else if (command.equals("CreateCollection")) {
					ch = new CreateCollection();
				} else if (command.equals("DeleteCollection")) {
					ch = new DeleteCollection();
				} else if (command.equals("MoveCollection")) {
					ch = new MoveCollection();
				} else if (command.equals("FolderSync")) {
					ch = new FolderSync();
				} else if (command.equals("FolderCreate")) {
					ch = new FolderCreate();
				} else if (command.equals("FolderDelete")) {
					ch = new FolderDelete();
				} else if (command.equals("FolderUpdate")) {
					ch = new FolderUpdate();
				} else if (command.equals("MoveItems")) {
					ch = new MoveItems();
				} else if (command.equals("GetItemEstimate")) {
					ch = new GetItemEstimate();
				} else if (command.equals("MeetingResponse")) {
					ch = new MeetingResponse();
				} else if (command.equals("Notify")) {
					ch = new Notify();
				} else if (command.equals("Ping")) {
					ch = new Ping();
				} else if (command.equals("Provision")) {
					ch = new Provision();
				} else if (command.equals("Search")) {
					ch = new Search();
				} else {
					log.severe("Posted Command " + command + " unknown!!");
				}
				if (ch != null) {
					ch.handle(this, backend, deviceid, protVers, log, user, password);
				}
				os.close();
				return;
			} catch (Exception e) {
				e.printStackTrace();
				sendError();
				BackendLogoff(deviceid);
				return;
			}
		} else if (requestMethod.equals("GET")) {
			addResponseHeader("Content-type", "text/html");
			sendResponseHeaders(0, 0);
			print("<BODY>\n");
			print("<h3>GET not supported</h3><p>\n");
			print("This is the z-push location and can only be accessed by Microsoft ActiveSync-capable devices.");
			print("</BODY>\n");
			os.close();
			BackendLogoff(deviceid);
			return;
		}
	}

	private void sendError() throws IOException {
		sendResponseHeaders(0, 0);
		print("<BODY>\n");
		print("<h3>Error</h3><p>\n");
		print("There was a problem processing the <i>$cmd</i> command from your PDA.\n");
		print("<p>Here is the debug output:<p><pre>\n");
		print("</pre>\n");
		print("</BODY>\n");
	}

	private void BackendLogoff(String deviceid) {
		Backend b = bends.get(deviceid);
		if (b != null) {
			b.logoff();
			bends.remove(deviceid);
		}
	}

	private static final String sval(String key, String def) {
		return key==null? def : key;
	}

	private final void print(String out) {
		try {
			os.write(out.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
