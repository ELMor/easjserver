package org.olmo.easjs.server.object;

import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPFolderHierarchy;
import org.w3c.dom.Node;

public class SyncFolder extends SyncObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5123609473494637146L;
	@Override
	public void encode(EASEncoder xo) {
		xo.element(CPFolderHierarchy.ServerEntryId, id);
		xo.element(CPFolderHierarchy.ParentId, parentid);
		xo.element(CPFolderHierarchy.DisplayName, displayname);
		xo.element(CPFolderHierarchy.Type, ""+type);
	}
	public SyncFolder(){
		
	}
	public SyncFolder(SyncObject c){
		id=c.id;
		parentid=c.parentid;
		type=c.type;
	}
	public SyncFolder(Node n){
		id=getTextSNN(n,CPFolderHierarchy.ServerEntryId);
		parentid=getTextSNN(n, CPFolderHierarchy.ParentId);
		displayname=getTextSNN(n, CPFolderHierarchy.DisplayName);
		type=getTextSNN(n, CPFolderHierarchy.Type);
	}
}
