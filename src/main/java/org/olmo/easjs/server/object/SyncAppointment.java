package org.olmo.easjs.server.object;

import java.util.Date;
import java.util.Vector;

import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPCalendar;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SyncAppointment extends SyncObject {
	private static final long serialVersionUID = -6436320348695909179L;
	public String subject,uid,organizername,organizeremail,location;
	public TimeZone timezone;
	public String sensitivity,busystatus,reminder,rtf,body,meetingstatus;
	public boolean alldayevent,bodytruncated;
	public String exceptionDeleted;
	public Date dtstamp,starttime,endtime;
	public SyncRecurrence recurrence;
	public Vector<SyncAttendee> attendees=null;
	public Vector<SyncAppointment> exceptions=null;
	public Date exceptionsstarttime=null;
	public Vector<String> categories=null;
	
	@Override
	public void encode(EASEncoder xo) {
		xo.element(CPCalendar.Timezone,timezone);
		xo.element(CPCalendar.DtStamp,encodeDate(dtstamp));
		xo.element(CPCalendar.StartTime,encodeDate(starttime));
		xo.element(CPCalendar.Subject,subject);
		xo.element(CPCalendar.UID,uid); //TODO STREAMER_TYPE_HEX?
		xo.element(CPCalendar.OrganizerName,organizername);
		xo.element(CPCalendar.OrganizerEmail,organizeremail);
		xo.element(CPCalendar.Location,location);
		xo.element(CPCalendar.EndTime,encodeDate(endtime));
		if(body!=null){
			xo.element(CPCalendar.BodyTruncated,bodytruncated);
			xo.element(CPCalendar.Body,body);
		}
		xo.element(CPCalendar.Sensitivity,sensitivity);
		xo.element(CPCalendar.BusyStatus,busystatus);
		xo.element(CPCalendar.AllDayEvent,alldayevent);
		xo.element(CPCalendar.ReminderMinsBefore,reminder);
		xo.element(CPCalendar.MeetingStatus,meetingstatus);

		if(categories!=null){
			xo.open(CPCalendar.Categories);
			for(int i=0;i<categories.size();i++)
				xo.element(CPCalendar.Category,categories.get(i));
			xo.close();
		}
		xo.element(CPCalendar.Rtf,rtf);
		xo.element(CPCalendar.Recurrence,recurrence);
		if(attendees!=null){
			xo.open(CPCalendar.Attendees);
			for(int i=0;i<attendees.size();i++)
				xo.element(CPCalendar.Attendee,attendees.get(i));
			xo.close();
		}
		if(exceptions!=null){
			xo.open(CPCalendar.Exceptions);
			for(int i=0;i<exceptions.size();i++)
				xo.element(CPCalendar.Exception,exceptions.get(i));
			xo.close();
		}
	}

	public SyncAppointment(Node n){
		if(n==null)
			return;
		timezone=new TimeZone(getTextSNN(n, CPCalendar.Timezone));
		dtstamp=parseDate(getTextSNN(n, CPCalendar.DtStamp));
		starttime=parseDate(getTextSNN(n, CPCalendar.StartTime));
		subject=getTextSNN(n, CPCalendar.Subject);
		uid=getTextSNN(n, CPCalendar.UID);
		organizername=getTextSNN(n, CPCalendar.OrganizerName);
		organizeremail=getTextSNN(n, CPCalendar.OrganizerEmail);
		location=getTextSNN(n, CPCalendar.Location);
		endtime=parseDate(getTextSNN(n, CPCalendar.EndTime));
		recurrence=new SyncRecurrence(getSNN(n, CPCalendar.Recurrence));
		sensitivity=getTextSNN(n, CPCalendar.Sensitivity);
		busystatus=getTextSNN(n, CPCalendar.BusyStatus);
		alldayevent=getBooSNN(n, CPCalendar.AllDayEvent);
		reminder=getTextSNN(n, CPCalendar.ReminderMinsBefore);
		rtf=getTextSNN(n, CPCalendar.Rtf);
		meetingstatus=getTextSNN(n, CPCalendar.MeetingStatus);
		
		Node atts=getSNN(n, CPCalendar.Attendees);
		if(atts!=null){
			NodeList nl=atts.getChildNodes();
			for(int i=0;i<nl.getLength();i++)
				attendees.add(new SyncAttendee(nl.item(i)));
		}
		
		body=getTextSNN(n, CPCalendar.Body);
		bodytruncated=getBooSNN(n, CPCalendar.BodyTruncated);
		
		Node excs=getSNN(n, CPCalendar.Exceptions);
		if(excs!=null){
			NodeList nl=excs.getChildNodes();
			for(int i=0;i<nl.getLength();i++){
				exceptions.add(new SyncAppointment(nl.item(i)));
			}
		}
		
		
		Node cats=getSNN(n, CPCalendar.Categories);
		if(cats!=null){
			NodeList nl=cats.getChildNodes();
			for(int i=0;i<nl.getLength();i++)
				categories.add(getTextSNN(nl.item(i), CPCalendar.Category));
		}
		
	}
	
}
