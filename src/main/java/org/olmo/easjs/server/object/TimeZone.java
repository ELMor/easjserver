package org.olmo.easjs.server.object;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.codec.binary.Base64;
import org.olmo.easjs.wbxml.eas.EASEncoder;

public class TimeZone extends SyncObject {

	private static final long serialVersionUID = 1L;
	
	Date day,stand;
	int utcOff;
	
	public TimeZone(int utc, Date d, Date s){
		utcOff=utc;
		day=d;
		stand=s;
	}
	
	public TimeZone(String s){
		byte buf[]=new Base64().decode(s);
		utcOff=byteArrayToInt(buf, 0);
		stand=SYSTEMTIMEToDate(buf, 52);
		day=SYSTEMTIMEToDate(buf, 88);
	}
	public String encode(){
		byte buf[]=new byte[108];
		for(int i=0;i<buf.length;i++)
			buf[i]=0;
		intToByteArray(utcOff, buf, 0);
		dateToSYSTEMTIME(stand, buf, 52);
		dateToSYSTEMTIME(day, buf, 88);
		return new String(new Base64().encode(buf));
	}
	
	public static void intToByteArray(int a, byte buf[], int off){
		for(int i=0;i<4;i++){
			buf[off+3-i]=(byte)(a >>> (i * 8));
		}
	}
	
	public static void wordToByteArray(int a, byte buf[], int off){
		for(int i=0;i<2;i++){
			buf[off+2-i]=(byte)(a >>> (i * 8));
		}
	}
	
	public static int byteArrayToInt(byte buf[], int off){
		int ret=0;
		for(int i =0; i < 4; i++){      
		   ret <<= 8;  
		   ret ^= (long)buf[off+i] & 0xFF;      
		}
		return ret;
	}
	
	public static int wordArrayToInt(byte buf[], int off){
		int ret=0;
		for(int i =0; i < 2; i++){      
		   ret <<= 8;  
		   ret ^= (long)buf[off+i] & 0xFF;      
		}
		return ret;
	}
	
	public static void dateToSYSTEMTIME(Date d, byte buf[], int off){
		Calendar c=new GregorianCalendar();
		c.setTime(d);
		wordToByteArray(c.get(Calendar.YEAR)-1900,buf,0+off);
		wordToByteArray(c.get(Calendar.MONTH),buf,2+off);
		wordToByteArray(c.get(Calendar.DAY_OF_MONTH),buf,4+off);
		wordToByteArray(c.get(Calendar.DATE),buf,6+off);
		wordToByteArray(c.get(Calendar.HOUR),buf,8+off);
		wordToByteArray(c.get(Calendar.MINUTE),buf,10+off);
		wordToByteArray(c.get(Calendar.SECOND),buf,12+off);
		wordToByteArray(0,buf,14+off);
	}

	public Date SYSTEMTIMEToDate(byte buf[], int off){
		Calendar c=new GregorianCalendar();
		c.set(wordArrayToInt(buf, 0+off), 
				wordArrayToInt(buf,2+off), 
				wordArrayToInt(buf,6+off), 
				wordArrayToInt(buf,8+off), 
				wordArrayToInt(buf,10+off), 
				wordArrayToInt(buf,12+off));
		return new Date(c.get(Calendar.DATE));
	}
	
	@Override
	public void encode(EASEncoder xo) {
		xo.chars(encode());
	}
}
