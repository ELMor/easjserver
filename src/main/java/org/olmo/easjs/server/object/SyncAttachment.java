package org.olmo.easjs.server.object;

import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPEmail;
import org.w3c.dom.Node;

public class SyncAttachment extends SyncObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3435535170684855518L;

	@Override
	public void encode(EASEncoder xo) {
		xo.element(CPEmail.AttMethod, ""+attmethod);
		xo.element(CPEmail.AttSize, ""+attsize);
		xo.element(CPEmail.DisplayName, displayname);
		xo.element(CPEmail.AttName, attname);
		xo.element(CPEmail.AttOid, attoid);
		xo.element(CPEmail.AttRemoved, ""+attremoved);
	}

	public int attmethod;
	public int attsize;
	public String displayname;
	public String attname;
	public String attoid;
	public boolean attremoved;

	public SyncAttachment(){
		
	}
	
	public SyncAttachment(Node n) {
		attmethod=getIntSNN(n, CPEmail.AttMethod);
		attsize=getIntSNN(n, CPEmail.AttSize);
		displayname=getTextSNN(n, CPEmail.DisplayName);
		attname=getTextSNN(n, CPEmail.AttName);
		attoid=getTextSNN(n, CPEmail.AttOid);
		attremoved=getBooSNN(n, CPEmail.AttRemoved);
	}
}
