package org.olmo.easjs.server.object;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.olmo.easjs.Cons;
import org.olmo.easjs.wbxml.eas.EASEncoder;

public class SyncObject extends NodeMgr implements Serializable, Comparable<SyncObject>,Cons{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8341023807037839816L;
	public static final Hashtable<String, String> objClassFold = new Hashtable<String, String>();
	static {
		objClassFold.put("Email","org.olmo.easjs.server.SyncMail");
		objClassFold.put("Contacts","org.olmo.easjs.server.SyncContact");
		objClassFold.put("Calendar","org.olmo.easjs.server.SyncAppointment");
		objClassFold.put("Tasks","org.olmo.easjs.server.SyncTask");
	};
	
	public static final String formatDateDashes = "yyyy-MM-dd hh:mm:ss.SSS";
	public static final String formatDate = "yyyyMMdd hhmmss"; //20100323T095420Z

	/**
	 * Identificador del servidor para el objeto
	 */
	public String id;
	/**
	 * Tipo de objeto
	 */
	public String type;
	/**
	 * Identificador del padre
	 */
	public String parentid;
	/**
	 * Como se muestra al usuario
	 */
	public String displayname;
	/**
	 * Flags. Normalmente indican el tipo de cambio al que se ha sometido al objeto
	 */
	public String flags;
	/**
	 * Clave de modificaci�n. Al comparar el objeto del servidor con el de la PDA se compara sus 'mod'
	 */
	public String mod=null;
	/**
	 * Para correo: si se ha leido.
	 */
	public String read=null;

	public int compareTo(SyncObject o){
		return id.compareTo(o.id);
	}

	public SyncObject(String id, String type, String flags, String mod, String parentid, String read){
		this.id=id;
		this.type=type;
		this.flags=flags;
		this.mod=mod;
		this.parentid=parentid;
		this.read=read;
	}
	
	public static Date parseDateDashes(String s) {
		if(s==null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(formatDateDashes);
		try {
			Date ret = sdf.parse(s);
			return ret;
		} catch (ParseException e) {
		}
		return null;
	}

	public static String encodeDateDashes(Date d) {
		if(d==null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(formatDateDashes);
		String ret=sdf.format(d);
		ret=ret.replace(' ', 'T');
		ret+="Z";
		return ret;
	}

	public static Date parseDate(String s) {
		if(s==null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
		try {
			s=s.replace('T', ' ');
			Date ret = sdf.parse(s);
			return ret;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String encodeDate(Date d) {
		if(d==null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
		String ret=sdf.format(d);
		ret=ret.replace(' ', 'T');
		ret+="Z";
		return ret;
	}

	public SyncObject() {
		id = parentid = displayname = null;
		type = "1";
	}

	public void encode(EASEncoder xo){
		throw new RuntimeException("Unimplemented");
	}

}
