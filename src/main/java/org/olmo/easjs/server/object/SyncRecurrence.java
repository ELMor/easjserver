package org.olmo.easjs.server.object;

import java.util.Date;

import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPEmail;
import org.w3c.dom.Node;

public class SyncRecurrence extends SyncObject {
	
	private static final long serialVersionUID = -3985777134499767029L;

	public SyncRecurrence(Node n){
		if(n==null)
			return;
		type=getTextSNN(n, CPEmail.Type);
		until=parseDateDashes(getTextSNN(n, CPEmail.Until));
		occurrences=getIntSNN(n, CPEmail.Occurrences);
		interval=getIntSNN(n, CPEmail.Interval);
		dayoffweek=getIntSNN(n, CPEmail.DayOfWeek);
		dayofmonth=getIntSNN(n, CPEmail.DayOfMonth);
		weekofmonth=getIntSNN(n, CPEmail.WeekOfMonth);
		monthofyear=getIntSNN(n, CPEmail.MonthOfYear);
	}
	/**
	 * 0 diario, 1 semanal, 2 mensual, 3 mensual en el n d�a, 5 anual, 6 anual en el n dia
	 * (requerido)
	 */
	public String type;
	/**
	 * Fin de la serie. Si se especifica, no puede haber occurrences.
	 */
	public Date until;
	/**
	 * Numero de veces que ocurre la serie (opcional)
	 */
	public int occurrences;
	/**
	 * Depende del type. valor maximo 999.
	 */
	public int interval;
	/**
	 * Dia de la semana:
	 * 1 Domingo, 2 Lunes, 4 Martes, 8 Miercoles, 16 Jueves, 32 Viernes, 64 Sabado
	 * 127 Ultimo dia del mes (valor especial si type=2,3,4,5) 
	 */
	public int dayoffweek;
	/**
	 * Obligatorio si type=2,3,5,6
	 */
	public int dayofmonth;
	/**
	 * Entre 1 y 5
	 */
	public int weekofmonth;
	/**
	 * Obligatorio si type=5,6. Valores de 1 a 12.
	 */
	public int monthofyear;

	@Override
	public void encode(EASEncoder xo) {
		xo.element(CPEmail.Type, type);
		xo.element(CPEmail.Until, encodeDateDashes(until));
		xo.element(CPEmail.Occurrences, ""+occurrences);
		xo.element(CPEmail.Interval, ""+interval);
		xo.element(CPEmail.DayOfWeek, ""+dayoffweek);
		xo.element(CPEmail.DayOfMonth, ""+dayofmonth);
		xo.element(CPEmail.WeekOfMonth, ""+weekofmonth);
		xo.element(CPEmail.MonthOfYear, ""+monthofyear);
	}
}
