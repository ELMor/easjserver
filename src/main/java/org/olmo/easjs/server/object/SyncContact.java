package org.olmo.easjs.server.object;

import java.util.Date;
import java.util.Vector;

import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPContacts;
import org.olmo.easjs.wbxml.eas.cp.CPContacts2;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SyncContact extends SyncObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1361407653031962790L;
	public Date anniversary;
	public String assistantname;
	public String assistnamephonenumber;
	public Date birthday;
	public String body;
	public String bodysize;
	public String bodytruncated;
	public String business2phonenumber;
	public String businesscity;
	public String businesscountry;
	public String businesspostalcode;
	public String businessstate;
	public String businessstreet;
	public String businessfaxnumber;
	public String businessphonenumber;
	public String carphonenumber;
	public Vector<String> categories=null;
	public Vector<String> children=null;
	public String companyname;
	public String department;
	public String email1address;
	public String email2address;
	public String email3address;
	public String fileas;
	public String firstname;
	public String home2phonenumber;
	public String homecity;
	public String homecountry;
	public String homepostalcode;
	public String homestate;
	public String homestreet;
	public String homefaxnumber;
	public String homephonenumber;
	public String jobtitle;
	public String lastname;
	public String middlename;
	public String mobilephonenumber;
	public String officelocation;
	public String othercity;
	public String othercountry;
	public String otherpostalcode;
	public String otherstate;
	public String otherstreet;
	public String pagernumber;
	public String radiophonenumber;
	public String spouse;
	public String suffix;
	public String title;
	public String webpage;
	public String yomicompanyname;
	public String yomifirstname;
	public String yomilastname;
	public String rtf;
	public String picture;
	public String nickname;
	public String custormerId;
	public String governmentId;
	public String imAddress;
	public String imAddress2;
	public String imAddress3;
	public String managerName;
	public String companyMainPhone;
	public String accountName;
	public String mms;
	
	@Override
	public void encode(EASEncoder xo) {
		if(children!=null){
			xo.open(CPContacts.Children);
			for(int i=0;i<children.size();i++)
				xo.element(CPContacts.Child,children.get(i));
			xo.close();
		}
		if(categories!=null){
			xo.open(CPContacts.Categories);
			for(int i=0;i<categories.size();i++){
				xo.element(CPContacts.Category,categories.get(i));
			}
			xo.close();
		}
		xo.element(CPContacts.Anniversary,encodeDateDashes(anniversary));
		xo.element(CPContacts.AssistantName,assistantname);
		xo.element(CPContacts.AssistnamePhoneNumber,assistnamephonenumber);
		xo.element(CPContacts.Birthday,encodeDateDashes(birthday));
		xo.element(CPContacts.BodyTruncated,bodytruncated);
		xo.element(CPContacts.BodySize,bodysize);
		xo.element(CPContacts.Body,body);
		xo.element(CPContacts.Business2PhoneNumber,business2phonenumber);
		xo.element(CPContacts.BusinessCity,businesscity);
		xo.element(CPContacts.BusinessCountry,businesscountry);
		xo.element(CPContacts.BusinessFaxNumber,businessfaxnumber);
		xo.element(CPContacts.BusinessPhoneNumber,businessphonenumber);
		xo.element(CPContacts.BusinessPostalCode,businesspostalcode);
		xo.element(CPContacts.BusinessState,businessstate);
		xo.element(CPContacts.BusinessStreet,businessstreet);
		xo.element(CPContacts.CarPhoneNumber,carphonenumber);
		xo.element(CPContacts.CompanyName,companyname);
		xo.element(CPContacts.Department,department);
		xo.element(CPContacts.Email1Address,email1address);
		xo.element(CPContacts.Email2Address,email2address);
		xo.element(CPContacts.Email3Address,email3address);
		xo.element(CPContacts.FileAs,fileas);
		xo.element(CPContacts.FirstName,firstname);
		xo.element(CPContacts.Home2PhoneNumber,home2phonenumber);
		xo.element(CPContacts.HomeCity,homecity);
		xo.element(CPContacts.HomeCountry,homecountry);
		xo.element(CPContacts.HomePostalCode,homepostalcode);
		xo.element(CPContacts.HomeState,homestate);
		xo.element(CPContacts.HomeStreet,homestreet);
		xo.element(CPContacts.HomeFaxNumber,homefaxnumber);
		xo.element(CPContacts.HomePhoneNumber,homephonenumber);
		xo.element(CPContacts.JobTitle,jobtitle);
		xo.element(CPContacts.LastName,lastname);
		xo.element(CPContacts.MiddleName,middlename);
		xo.element(CPContacts.MobilePhoneNumber,mobilephonenumber);
		xo.element(CPContacts.OfficeLocation,officelocation);
		xo.element(CPContacts.OtherCity,othercity);
		xo.element(CPContacts.OtherCountry,othercountry);
		xo.element(CPContacts.OtherPostalCode,otherpostalcode);
		xo.element(CPContacts.OtherState,otherstate);
		xo.element(CPContacts.OtherStreet,otherstreet);
		xo.element(CPContacts.PagerNumber,pagernumber);
		xo.element(CPContacts.RadioPhoneNumber,radiophonenumber);
		xo.element(CPContacts.Spouse,spouse);
		xo.element(CPContacts.Suffix,suffix);
		xo.element(CPContacts.Title,title);
		xo.element(CPContacts.WebPage,webpage);
		xo.element(CPContacts.YomiCompanyName,yomicompanyname);
		xo.element(CPContacts.YomiFirstName,yomifirstname);
		xo.element(CPContacts.YomiLastName,yomilastname);
		xo.element(CPContacts.Rtf,rtf);
		xo.element(CPContacts.Picture,picture);
		xo.element(CPContacts2.NickName,nickname);
		xo.element(CPContacts2.CustomerId,custormerId);
		xo.element(CPContacts2.GovernmentId,governmentId);
		xo.element(CPContacts2.IMAddress,imAddress);
		xo.element(CPContacts2.IMAddress2,imAddress2);
		xo.element(CPContacts2.IMAddress3,imAddress3);
		xo.element(CPContacts2.ManagerName,managerName);
		xo.element(CPContacts2.CompanyMainPhone,companyMainPhone);
		xo.element(CPContacts2.AccountName,accountName);
		xo.element(CPContacts2.MMS,mms);
	}

	public SyncContact(Node n) {
		if (n == null)
			return;
		anniversary=parseDateDashes(getTextSNN(n, CPContacts.Anniversary));
		assistantname=getTextSNN(n, CPContacts.AssistantName);
		assistnamephonenumber=getTextSNN(n, CPContacts.AssistnamePhoneNumber);
		birthday=parseDateDashes(getTextSNN(n, CPContacts.Birthday));
		body=getTextSNN(n, CPContacts.Body);
		bodysize=getTextSNN(n, CPContacts.BodySize);
		bodytruncated=getTextSNN(n, CPContacts.BodyTruncated);
		business2phonenumber=getTextSNN(n, CPContacts.Business2PhoneNumber);
		businesscity=getTextSNN(n, CPContacts.BusinessCity);
		businesspostalcode=getTextSNN(n, CPContacts.BusinessPostalCode);
		businessstate=getTextSNN(n, CPContacts.BusinessState);
		businessstreet=getTextSNN(n, CPContacts.BusinessStreet);
		businessfaxnumber=getTextSNN(n, CPContacts.BusinessFaxNumber);
		businessphonenumber=getTextSNN(n, CPContacts.BusinessPhoneNumber);
		carphonenumber=getTextSNN(n, CPContacts.CarPhoneNumber);
		companyname=getTextSNN(n, CPContacts.CompanyName);
		department=getTextSNN(n, CPContacts.Department);
		email1address=getTextSNN(n, CPContacts.Email1Address);
		email2address=getTextSNN(n, CPContacts.Email2Address);
		email3address=getTextSNN(n, CPContacts.Email3Address);
		fileas=getTextSNN(n, CPContacts.FileAs);
		firstname=getTextSNN(n, CPContacts.FirstName);
		home2phonenumber=getTextSNN(n, CPContacts.Home2PhoneNumber);
		homecity=getTextSNN(n, CPContacts.HomeCity);
		homecountry=getTextSNN(n, CPContacts.HomeCountry);
		homepostalcode=getTextSNN(n, CPContacts.HomePostalCode);
		homestate=getTextSNN(n, CPContacts.HomeState);
		homestreet=getTextSNN(n, CPContacts.HomeStreet);
		homefaxnumber=getTextSNN(n, CPContacts.HomeFaxNumber);
		homephonenumber=getTextSNN(n, CPContacts.HomePhoneNumber);
		jobtitle=getTextSNN(n, CPContacts.JobTitle);
		lastname=getTextSNN(n, CPContacts.LastName);
		middlename=getTextSNN(n, CPContacts.MiddleName);
		mobilephonenumber=getTextSNN(n, CPContacts.MobilePhoneNumber);
		officelocation=getTextSNN(n, CPContacts.OfficeLocation);
		othercity=getTextSNN(n, CPContacts.OtherCity);
		othercountry=getTextSNN(n, CPContacts.OtherCountry);
		otherpostalcode=getTextSNN(n, CPContacts.OtherPostalCode);
		otherstate=getTextSNN(n, CPContacts.OtherState);
		otherstreet=getTextSNN(n, CPContacts.OtherStreet);
		pagernumber=getTextSNN(n, CPContacts.PagerNumber);
		radiophonenumber=getTextSNN(n, CPContacts.RadioPhoneNumber);
		spouse=getTextSNN(n, CPContacts.Spouse);
		suffix=getTextSNN(n, CPContacts.Suffix);
		title=getTextSNN(n, CPContacts.Title);
		webpage=getTextSNN(n, CPContacts.WebPage);
		yomicompanyname=getTextSNN(n, CPContacts.YomiCompanyName);
		yomifirstname=getTextSNN(n, CPContacts.YomiFirstName);
		yomilastname=getTextSNN(n, CPContacts.YomiLastName);
		rtf=getTextSNN(n, CPContacts.Rtf);
		picture=getTextSNN(n, CPContacts.Picture);
		nickname=getTextSNN(n, CPContacts2.NickName);
		custormerId=getTextSNN(n, CPContacts2.CustomerId);
		governmentId=getTextSNN(n, CPContacts2.GovernmentId);
		imAddress=getTextSNN(n, CPContacts2.IMAddress);
		imAddress2=getTextSNN(n, CPContacts2.IMAddress2);
		imAddress3=getTextSNN(n, CPContacts2.IMAddress3);
		managerName=getTextSNN(n, CPContacts2.ManagerName);
		companyMainPhone=getTextSNN(n, CPContacts2.CompanyMainPhone);
		accountName=getTextSNN(n, CPContacts2.AccountName);
	    mms=getTextSNN(n, CPContacts2.MMS);	
		
		Node catNode=getSNN(n, CPContacts.Categories);
		if(catNode!=null){
			categories=new Vector<String>();
			NodeList catList=catNode.getChildNodes();
			for(int i=0;i<catList.getLength();i++){
				categories.add(catList.item(i).getTextContent());
			}
		}
		
		Node childrenNode=getSNN(n, CPContacts.Children);
		if(childrenNode!=null){
			children=new Vector<String>();
			NodeList attList=childrenNode.getChildNodes();
			for(int i=0;i<attList.getLength();i++){
				children.add(attList.item(i).getTextContent());
			}
		}
		
	}
}
