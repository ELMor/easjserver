package org.olmo.easjs.server.object;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;

public class NodeMgr {
	/**
	 * Get subnode of 'n'
	 * @param n Parent node
	 * @param name Name of child node
	 * @return Subnode
	 */
	public Node getSNN(Node n, String name){
		try {
			name = getLocalName(name);
			XPath xpath = XPathFactory.newInstance().newXPath();
			return (Node) xpath.evaluate(name, n, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private String getLocalName(String name) {
		int ndx=name.indexOf(':');
		if(ndx>0)
			name=name.substring(ndx+1);
		return name;
	}
	
	/**
	 * Get Text for subnode of 'n'
	 * @param n Node parent
	 * @param name Name of child node
	 * @return Contents of child node
	 */
	public String getTextSNN(Node n, String name){
		
		Node r=getSNN(n, name);
		if(r==null)
			return null;
		return r.getTextContent();
	}
	
	/**
	 * Get number for subnode of 'n'
	 * @param n Node parent
	 * @param name Name of child node
	 * @return Integer contained  child node
	 */
	public int getIntSNN(Node n, String name){
		String r=getTextSNN(n, name);
		try {
			return Integer.parseInt(r);
		}catch(Exception e){
			return 0;
		}
	}
	
	public boolean getBooSNN(Node n, String name){
		String r=getTextSNN(n, name);
		if(r==null)
			return false;
		if(r.equals("1"))
			return true;
		if(r.equalsIgnoreCase("true"))
			return true;
		return false;
	}
	

}
