package org.olmo.easjs.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.olmo.easjs.Conf;

import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

public class ServerTypeSUN implements ServerType {
	
	public void httpServerSetup() {
		HttpServer server=null;
		try {
			server = HttpServer.create();
			initServer(server,80);
		} catch (IOException e) {
			throw new IllegalStateException("Could not start server", e);
		}
	}

	public void httpsServerSetup() {
		//SSL & Https configuration
		HttpsConfigurator httpsConfigurator1;
		try {
			KeyStore keystore = KeyStore.getInstance("JKS");
			keystore.load(
					Class.class.getResourceAsStream(Conf.keyStoreName), 
					Conf.keyStorePwd.toCharArray());
		
			KeyStore truststore = KeyStore.getInstance("JKS");
			truststore.load(
					Class.class.getResourceAsStream(Conf.trustStoreName), 
					Conf.trustStorePwd.toCharArray());
		
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(keystore, Conf.keyStorePwd.toCharArray());
		
			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(truststore);
		
			SSLContext ssl = SSLContext.getInstance("TLS");
			
			KeyManager kms[]=kmf.getKeyManagers();
			TrustManager tms[]=tmf.getTrustManagers();
			ssl.init(kms, tms, null);
		
			httpsConfigurator1 = new HttpsConfigurator(ssl) {
				@Override
				public void configure(HttpsParameters param) {
				}
			};
		} catch (Exception e1) {
			throw new IllegalStateException(
					"Could not initialize httpsConfigurator", e1);
		}
		HttpsConfigurator httpsConfigurator = httpsConfigurator1;
		HttpsServer server=null;
		try {
			server = HttpsServer.create();
			server.setHttpsConfigurator(httpsConfigurator);
			initServer(server,443);
		} catch (IOException e) {
			throw new IllegalStateException("Could not start server", e);
		}
	}

	private void initServer(HttpServer server, int port) throws IOException {
		server.bind(new InetSocketAddress(port), Conf.maxThreads);
		server.createContext(ctx,new EASHandler());
		ExecutorService threads = Executors.newFixedThreadPool(Conf.maxThreads);
		server.setExecutor(threads);
		server.start();
	}

}
