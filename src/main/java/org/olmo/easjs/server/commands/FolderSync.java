package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.Conf;
import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.backend.HierExport;
import org.olmo.easjs.backend.HierImport;
import org.olmo.easjs.backend.HierImportStream;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncObject;
import org.olmo.easjs.store.StateMachine;
import org.olmo.easjs.wbxml.eas.EASDecoder;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPFolderHierarchy;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class FolderSync extends CommandHandler {

	@SuppressWarnings("unchecked")
	@Override
	public void handle(
			EASHandler x, 
			Backend backend, 
			String deviceid, 
			String protocolversion, 
			Logger log, String user, String pwd) 
	throws SAXException,IOException, ParserConfigurationException, XPathExpressionException 
	{
		EASDecoder dec=new EASDecoder(x.getRequestBody());
		Hashtable<String, String>map = new Hashtable<String, String>();
		//Obtenemos synckey enviado por la pda
		String synckey=dec.xprText("/FolderSync/SyncKey");
		//Obtener syncstate (grabado de la anterior sesi�n)
		StateMachine sm=new StateMachine();
		Vector<SyncObject> syncstate=sm.getSyncState(synckey);
		if(syncstate==null)
			syncstate=new Vector<SyncObject>();
		//Folders ya recorridos
		Vector<String>seenfolders=(Vector<String>)Conf.st.get("s"+synckey);
		if(seenfolders==null)
			seenfolders=new Vector<String>();
		//Nueva clave de sincronizacion
		String newsynckey=sm.getNewSyncKey(synckey);
		//Lista de Folders
		NodeList cmds=dec.xprNodeList(CPFolderHierarchy.FolderSync,CPFolderHierarchy.Changes);
		
		for(int cmdn=0;cmdn<cmds.getLength();cmdn++){
			Node cmd=cmds.item(cmdn);
			String tag=cmd.getLocalName();
			NodeList folders=cmd.getChildNodes();
			for(int foldern=0;foldern<folders.getLength();foldern++){
				SyncFolder folder=new SyncFolder(folders.item(foldern));
				
				HierImport importer=backend.getHierImporter();
				importer.Config(syncstate);
				
				String serverid=null;
				if(tag.equals("Add")||tag.equals("Change")){
					serverid=importer.folderChange(folder);
					seenfolders.add(serverid);
				}else if(tag.equals("Delete")){
					serverid=importer.folderDeletion(folder);
					seenfolders.remove(serverid);
				}
				if(serverid!=null){
					map.put(serverid, folder.id);
				}
			}
		}
	    // We have processed incoming foldersync requests, now send the PIM
	    // our changes
		HierImportStream importer=new HierImportStream(backend);
		HierExport exporter=backend.getHierExporter(importer,null,0,syncstate,BACKEND_DISCARD_DATA,0);
		while(exporter.synchronize())
			;
		x.sendResponseHeaders(0, 0);
		EASEncoder xo=new EASEncoder("FolderHierarchy",x.getResponseBody());
		xo.open(CPFolderHierarchy.FolderSync);
		xo.element(CPFolderHierarchy.Status,"1");
		xo.element(CPFolderHierarchy.SyncKey,newsynckey);
		xo.open(CPFolderHierarchy.Changes);
		xo.element(CPFolderHierarchy.Count,""+importer.getSize());
		if(importer.getSize()>0){
			Vector<SyncFolder> vsf=importer.getChanged();
			for(int i=vsf.size()-1;i>=0;i--){
				SyncFolder sf=vsf.elementAt(i);
				if(sf.id!=null && seenfolders.contains(sf.id)){
					xo.element(CPFolderHierarchy.Update,sf);
				}else{
					xo.element(CPFolderHierarchy.Add,sf);
					seenfolders.add(sf.id);
				}
			}
			for(SyncFolder sf:importer.getDeleted()){
				xo.open(CPFolderHierarchy.Remove);
				xo.element(CPFolderHierarchy.ServerEntryId,sf);
				xo.close();
			}
		}
		xo.close();
		xo.close();
		syncstate=exporter.getState();
		sm.setSyncState(newsynckey, syncstate);
		sm.setSyncState("s"+newsynckey, seenfolders);
		xo.getBytes();
	}
}
