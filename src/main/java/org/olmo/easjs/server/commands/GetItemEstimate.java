package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.backend.ContExport;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.server.object.Coleccion;
import org.olmo.easjs.server.object.SyncObject;
import org.olmo.easjs.store.StateMachine;
import org.olmo.easjs.wbxml.eas.EASDecoder;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPAirSync;
import org.olmo.easjs.wbxml.eas.cp.CPItemEstimate;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class GetItemEstimate extends CommandHandler {

	@Override
	public void handle(EASHandler x, Backend backend, 
			String deviceid, String protocolversion, Logger log, String user, String pwd) throws SAXException,
			IOException, ParserConfigurationException, XPathExpressionException {
		EASDecoder dec=new EASDecoder(x.getRequestBody());
		Vector<Coleccion> cols=new Vector<Coleccion>();
		// /GetItemEstimate/Folders/Folder
		NodeList nl=dec.xprNodeList(CPItemEstimate.GetItemEstimate,CPItemEstimate.Folders,CPItemEstimate.Folder);
		
		for(int i=0;i<nl.getLength();i++){
			Coleccion col=new Coleccion();
			Node folder=nl.item(i);
			col.type=getTextSNN(folder, CPItemEstimate.FolderType);
			col.id=getTextSNN(folder, CPItemEstimate.FolderId);
			col.filter=getIntSNN(folder, CPAirSync.FilterType);
			col.syncKey=getTextSNN(folder, CPAirSync.SyncKey);
			if(col.id==null)
				col.id=getFolderID(deviceid, col.type);
			cols.add(col);
		}
		x.sendResponseHeaders(0, 0);
		EASEncoder xo=new EASEncoder("ItemEstimate",x.getResponseBody());
		xo.open(CPItemEstimate.GetItemEstimate);
		for(int i=0;i<cols.size();i++){
			Coleccion col=cols.elementAt(i);
			xo.open(CPItemEstimate.Response);
			xo.element(CPItemEstimate.Status,"1");
			xo.open(CPItemEstimate.Folder);
			xo.element(CPItemEstimate.FolderType,col.type);
			xo.element(CPItemEstimate.FolderId,col.id);
			
			StateMachine sm=new StateMachine();
			Vector<SyncObject> syncstate=sm.getSyncState(col.syncKey);
			ContExport exporter=backend.getContExporter(col.id,null, col.type, col.filter, syncstate, 0, 0);
			xo.element(CPItemEstimate.Estimate,""+exporter.getChangeCount());
			xo.close();
			xo.close();
		}
		xo.close();
		xo.getBytes();
	}

}
