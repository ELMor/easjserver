package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.wbxml.eas.EASDecoder;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.xml.sax.SAXException;


public class Notify extends CommandHandler {

	@Override
	public void handle(EASHandler x, Backend backend, 
			String deviceid, String protocolversion, Logger log, String user, String pwd) throws SAXException,
			IOException, ParserConfigurationException, XPathExpressionException {
		EASDecoder dec=new EASDecoder(x.getRequestBody());
		if(dec.xprNode("/Notify/DeviceInfo")==null)
			return;
		x.sendResponseHeaders(0, 0);
		EASEncoder xo=new EASEncoder("AirNotify",x.getResponseBody());
		xo.open("Notify");
		xo.element("Status","1");
		xo.openclose("ValidCarrierProfiles");
		xo.close();
		xo.getBytes();
	}

}
