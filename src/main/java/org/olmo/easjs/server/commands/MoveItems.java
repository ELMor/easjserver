package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.backend.ContImport;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.wbxml.eas.EASDecoder;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class MoveItems extends CommandHandler {

	class Move {
		String srcmsgid;
		String srcfldid;
		String dstfldid;
		public Move(String mid,String sfi,String dfi){
			srcmsgid=mid;
			srcfldid=sfi;
			dstfldid=dfi;
		}
	}
	@Override
	public void handle(
			EASHandler x, 
			Backend backend, 
			String deviceid, 
			String protocolversion, 
			Logger log, String user, String pwd) 
	throws SAXException,IOException, ParserConfigurationException, XPathExpressionException 
	{
		EASDecoder dec=new EASDecoder(x.getRequestBody());
		Node moves=dec.xprNode("/Moves/Move");
		NodeList moveList=moves.getChildNodes();
		Vector<Move> moveData=new Vector<Move>();
		for(int i=0;i<moveList.getLength();i++){
			moveData.add(new Move(
				dec.xprText("/Moves/Move[/"+(i+1)+"]/SrcMsgId"),
				dec.xprText("/Moves/Move[/"+(i+1)+"]/SrcFldId"),
				dec.xprText("/Moves/Move[/"+(i+1)+"]/DstFldId")
			));
		}
		x.sendResponseHeaders(0, 0);
		EASEncoder xo=new EASEncoder("Move",x.getResponseBody());
		xo.open("Moves");
		for(int i=0;i<moveData.size();i++){
			Move m=moveData.get(i);
			xo.open("Response");
			xo.element("SrcMsgId",m.srcmsgid);
			ContImport ds=backend.getContImporter(m.srcfldid,null,0);
			String result=ds.move(m.srcfldid, m.dstfldid);
			xo.element("Status",(result!=null?"3":"1"));
			xo.element("DstMsgId",result);
			xo.close();
		}
		xo.close();
		xo.getBytes();
	}

}
