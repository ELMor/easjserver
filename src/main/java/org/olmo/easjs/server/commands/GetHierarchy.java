package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.server.object.SyncFolder;
import org.olmo.easjs.server.object.SyncObject;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.xml.sax.SAXException;


public class GetHierarchy extends CommandHandler {

	@Override
	public void handle(EASHandler x, Backend backend, 
			String deviceid, String protocolversion, Logger log, String user, String pwd) throws SAXException,
			IOException, ParserConfigurationException, XPathExpressionException {
		
		//Ojo, no hay datos en is �omitir?
		Vector<SyncObject> folders=backend.getHierarchy();
		if(folders==null || folders.size()==0)
			return;
		saveFolderData(deviceid, folders);
		x.sendResponseHeaders(0, 0);
		EASEncoder xo=new EASEncoder("FolderHierarchy",x.getResponseBody());
		xo.open("Folders");
		for(SyncObject c:folders){
			SyncFolder sf=new SyncFolder(c);
			sf.encode(xo);
		}
		xo.close();
		xo.getBytes();
	}

}
