package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.Conf;
import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.backend.ContExport;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.server.commands.objects.PingCol;
import org.olmo.easjs.server.commands.objects.PingStatus;
import org.olmo.easjs.wbxml.eas.EASDecoder;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPAirSync;
import org.olmo.easjs.wbxml.eas.cp.CPPing;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Ping extends CommandHandler {

	@Override
	public void handle(EASHandler x, Backend backend, 
			String deviceid, String protocolversion, Logger log, String user, String pwd) throws SAXException,
			IOException, ParserConfigurationException, XPathExpressionException {

		log.info("Ping received");
		//Parse input
		EASDecoder dec=new EASDecoder(x.getRequestBody());
		
		int timeout=5;
		int lifetime=0;
		Vector<PingCol> pcols=new Vector<PingCol>();

		PingStatus savedData=(PingStatus)Conf.st.get(deviceid);
		if(savedData!=null){
			lifetime=savedData.lifetime;
			pcols=savedData.collections;
		}else{
			savedData=new PingStatus();
		}
		if(dec.getDoc()!=null){
			Node ping=dec.xprNode("/Ping");
			lifetime=getIntSNN(ping, "LifeTime");
	
			NodeList folders=dec.xprNodeList(CPPing.Ping,CPPing.Folders,CPPing.Folder);
	
			Vector<PingCol> saved_collections=pcols;
			pcols=new Vector<PingCol>();
			
			for(int i=0;i<folders.getLength();i++){
				PingCol pingCol=new PingCol();
				Node folder=folders.item(i);
				pingCol.fid=getTextSNN(folder, CPAirSync.ServerEntryId);
				pingCol.className=getTextSNN(folder, CPPing.FolderType);
				pingCol.state=null;
				//Try to find old state in saved states
				for(PingCol scol:saved_collections){
					if(scol.fid.equals(pingCol.fid)){
						pingCol.state=scol.state;
						break;
					}
				}
				if(pingCol.state==null)
					log.info("Empty state for "+pingCol.className);
				ContExport exporter=backend.getContExporter(
						pingCol.fid,null, null, 0, 
						pingCol.state, BACKEND_DISCARD_DATA, 0);
				while(exporter.synchronize())
					;
				pingCol.state=exporter.getState();
				pcols.add(pingCol);
				//Notificamos al backend que se va a monitorizar el folder.
				backend.wantMonitorFolder(pingCol.fid);
			}
		}
		boolean dataavailable=false;
		int pingstatus=-1;
		int error=-1;
		Hashtable<String, Integer> changes=new Hashtable<String, Integer>();
		x.sendResponseHeaders(0, 0);
		log.info("Waiting for something to happen");
		//Permitir al backend gestionar el ping de la mejor forma posible
		for(int n=0;n<lifetime/timeout;n++){
			//Check remote wipe status
			int rwstatus=backend.getDeviceRWStatus(user, pwd, deviceid);
			if(rwstatus==PROVISION_RWSTATUS_PENDING ||
					rwstatus==PROVISION_RWSTATUS_WIPED){
				pingstatus=7;
				break;
			}
			if(pcols.size()==0){
				error=1;
				break;
			}
			for(int i=0;i<pcols.size();i++){
				PingCol pc=pcols.get(i);
				ContExport exporter=backend.getContExporter(pc.fid,null, null, 0, pc.state, 1, 0);
				int changecount=exporter.getChangeCount();
				if(changecount>0){
					dataavailable=true;
					changes.put(pc.fid, changecount);
				}
				while(exporter.synchronize())
					;
				pc.state=exporter.getState();
			}
			if(dataavailable){
				log.info("Found change");
				break;
			}
			try {
				Thread.sleep(timeout*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//No se como conseguir que el timeout del socket no se produzca
			OutputStream os=x.getResponseBody();
		}
		OutputStream os=x.getResponseBody();
		EASEncoder xo=new EASEncoder("Ping",os);
		xo.open(CPPing.Ping);
		String cnt=null;
		if(error!=-1){
			cnt="3";
		}else if(pingstatus!=-1){
			cnt=""+pingstatus;
		}else if(changes.size()>0){
			cnt="2";
		}else{
			cnt="1";
		}
		xo.element(CPPing.Status,cnt);
		xo.open(CPPing.Folders);
		for(PingCol pc:pcols){
			if(pc.fid!=null){
				xo.element(CPPing.Folder,pc.fid);
			}
		}
		xo.close();
		xo.close();
		savedData.collections=pcols;
		savedData.lifetime=lifetime;
		Conf.st.put(deviceid, savedData);
		xo.getBytes();
	}

}
