package org.olmo.easjs.server.commands;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.olmo.easjs.Cons;
import org.olmo.easjs.backend.Backend;
import org.olmo.easjs.backend.ContExport;
import org.olmo.easjs.backend.ContImportStream;
import org.olmo.easjs.server.CommandHandler;
import org.olmo.easjs.server.EASHandler;
import org.olmo.easjs.server.object.Coleccion;
import org.olmo.easjs.server.object.SyncAppointment;
import org.olmo.easjs.server.object.SyncContact;
import org.olmo.easjs.server.object.SyncMail;
import org.olmo.easjs.server.object.SyncObject;
import org.olmo.easjs.server.object.SyncTask;
import org.olmo.easjs.store.StateMachine;
import org.olmo.easjs.wbxml.eas.EASDecoder;
import org.olmo.easjs.wbxml.eas.EASEncoder;
import org.olmo.easjs.wbxml.eas.cp.CPAirSync;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Sync extends CommandHandler implements Cons{

	@Override
	public void handle(EASHandler x, Backend backend,
			String devid, String protocolversion, Logger log, String user, String pwd) throws SAXException,
			IOException, ParserConfigurationException, XPathExpressionException {
		//Decodificamos la entrada
		EASDecoder dec=new EASDecoder(x.getRequestBody());
		
		//Creamos stateMachine
		StateMachine sm=new StateMachine();
		
		//Acumularemos los Folder que vengan en la entrada aqu�
		Vector<Coleccion> cols=new Vector<Coleccion>();
		
		//ContImport importer=null;
		//ContExport exporter=null;
		
		// /Synchronize/Folders/Folder
		NodeList folders=dec.xprNodeList(CPAirSync.Synchronize,CPAirSync.Collections,CPAirSync.Collection);
		for(int i=0;i<folders.getLength();i++){
			Node folder=folders.item(i);
			Coleccion col=new Coleccion();
			col.trunc=TRUNCATION_ALL;
			col.clientIds=new Hashtable<String, SyncObject>();
			col.fetchIds=new Vector<String>();
			col.type=getTextSNN(folder, CPAirSync.Class);
			col.syncKey=getTextSNN(folder, CPAirSync.SyncKey);
			col.id=getTextSNN(folder, CPAirSync.CollectionId);
			col.deletesAsMoves=getSNN(folder,CPAirSync.DeletesAsMoves) != null ? true:false;
			col.getChanges=getSNN(folder,CPAirSync.GetChanges)!=null ? true : false;
			col.maxItems=getIntSNN(folder, CPAirSync.MaxItems);
			Node options=getSNN(folder, CPAirSync.Options);
			if(options!=null){
				col.filter=getIntSNN(options, CPAirSync.FilterType);
				col.trunc=getIntSNN(options, CPAirSync.Truncation);
				col.rtfTruncation=getIntSNN(options, CPAirSync.RtfTruncation);
				col.mimeSupport=getTextSNN(options, CPAirSync.MIMESupport);
				col.mimeTruncation=getIntSNN(options, CPAirSync.MIMETruncation);
				col.conflict=getIntSNN(options, CPAirSync.Conflict);
			}
	        // compatibility mode - get folderid from the state directory
			if(col.id==null)
				col.id=getFolderID(devid, col.type);
	        // compatibility mode - set default conflict behavior if no conflict 
			//resolution algorithm is set (OVERWRITE_PIM)
			if(col.conflict==0)
				col.conflict=1;
			
	        // Get our sync state for this collection
			col.syncstate=sm.getSyncState(col.syncKey);
			if(col.syncstate==null)
				col.syncstate=new Vector<SyncObject>();
			
			// �Vienen cambios desde la PDA?
			Node cmds=getSNN(folder, CPAirSync.Commands);
			if(cmds!=null){
				log.info("\tImportando cambios");
				col.importer=backend.getContImporter(col.id,col.syncstate, col.conflict);
				int nchanges=0;
				NodeList performList=cmds.getChildNodes();
				for(nchanges=0;nchanges<performList.getLength();nchanges++){
					Node perform=performList.item(nchanges);
					//Fue creado el objeto no en la pda
					String serverid=getTextSNN(perform, CPAirSync.ServerEntryId);
					//Se creo el objeto en la pda
					String clientid=getTextSNN(perform, CPAirSync.ClientEntryId);
					Node data=getSNN(perform, CPAirSync.Data);
					SyncObject appdata=null;
					if(data!=null){
						if(col.type.equals("Email")){
							appdata=new SyncMail(data);
						}else if(col.type.equals("Contacts")){
							appdata=new SyncContact(data);
						}else if(col.type.equals("Calendar")){
							appdata=new SyncAppointment(data);
						}else if(col.type.equals("Tasks")){
							appdata=new SyncTask(data);
						}else {
							String str="Class "+col.type+" unknown!";
							log.severe(str);
							throw new RuntimeException(str);
						}
					}
					String action=perform.getNodeName();
					if(action.equals(CPAirSync.Change)){
						log.info("\t\tModificando "+serverid);
						if(appdata!=null){
							if(appdata.read!=null)
								col.importer.readFlag(serverid, appdata.read);
							else
								col.importer.change(serverid, appdata);
							col.importedChanges=true;
						}
					}else if(action.equals(CPAirSync.Add)){
						log.info("\t\tCreando ClientId "+clientid);
						if(appdata!=null){
							SyncObject created=col.importer.change(null, appdata);
							if(clientid!=null && created!=null){
								col.clientIds.put(clientid, created);
								col.importedChanges=true;
							}
						}
					}else if(action.equals(CPAirSync.Delete)){
						log.info("\t\tBorrando "+serverid);
						if(col.deletesAsMoves){
							String folderid=backend.getWasteBasket();
							if(folderid!=null){
								col.importer.move(serverid, folderid);
								col.importedChanges=true;
							}
						}
						col.importer.delete(serverid);
						col.importedChanges=true;
					}else if(action.equals(CPAirSync.Fetch)){
						log.info("\t\tObteniendo "+serverid);
						col.fetchIds.add(serverid);
					}
				}
				col.syncstate=col.importer.getState();
			}
			cols.add(col);
		}
		
		//Enviamos los cambios a la PDA
		x.sendResponseHeaders(0, 0);
		EASEncoder xo=new EASEncoder("AirSync",x.getResponseBody());
		xo.open(CPAirSync.Synchronize,CPAirSync.Collections);
		for(Coleccion col:cols){
            // Get a new sync key to output to the client if any changes have been 
			//requested or have been sent
			if(col.importedChanges ||
					col.getChanges ||
					col.syncKey.equals("0")){
				col.newSyncKey=sm.getNewSyncKey(col.syncKey);
			}
			xo.open(CPAirSync.Collection);
			xo.element(CPAirSync.Class,col.type);
			xo.element(CPAirSync.SyncKey,col.newSyncKey!=null ? col.newSyncKey : col.syncKey);
			xo.element(CPAirSync.CollectionId,col.id);
			xo.element(CPAirSync.Status,"1");

			//check the mimesupport because we need it for advanced emails
			boolean mimes=col.mimeSupport !=null;
            // Output server IDs for new items we received from the PDA
			if(col.clientIds.size()>0 || col.fetchIds.size()>0){
				log.info("\t\tEnviando a la PDA ");
				xo.open(CPAirSync.Responses);
				for(Enumeration<String> e=col.clientIds.keys();e.hasMoreElements();){
					String clientid=e.nextElement();
					xo.open(CPAirSync.Add);
					xo.element(CPAirSync.ClientEntryId,clientid);
					xo.element(CPAirSync.ServerEntryId,col.clientIds.get(clientid).id);
					xo.element(CPAirSync.Status,"1");
					xo.close();
				}
				for(int ii=0;ii<col.fetchIds.size();ii++){
					String serverentryid=col.fetchIds.elementAt(ii);
					SyncObject fObj=backend.fetch(col.id, serverentryid, mimes);
					if(fObj!=null){
						xo.open(CPAirSync.Fetch);
						xo.element( CPAirSync.ServerEntryId,serverentryid,
									CPAirSync.Status,"1");
						xo.element( CPAirSync.Data, fObj);
						xo.close();
					}
				}
				xo.close();
			}
			ContExport exporter=null;
			if(col.getChanges){
                // Use the state from the importer, as changes may have already happened
				exporter=backend.getContExporter(col.id,col.importer, col.type, col.filter, col.syncstate, 0, col.trunc);
				int changecount=exporter.getChangeCount();
				if(changecount>col.maxItems){
					xo.openclose(CPAirSync.MoreAvailable);
				}
				xo.openCond(CPAirSync.Commands);
				col.importer=new ContImportStream(backend,col.id,xo,SyncObject.objClassFold.get(col.type));
				exporter.setContImport(col.importer);
				int n=0;
				while(true){
					boolean result=exporter.synchronize();
					if(!result)
						break;
					if(++n>=col.maxItems){
						break;
					}
				}
				xo.close();
			}
			xo.close();
			if(col.newSyncKey!=null){
				Vector<SyncObject> state=null;
				if(exporter!=null){
					state=exporter.getState();
				}else if(col.importer!=null){
					state=col.importer.getState();
				}else if(col.syncKey.equals("0")){
					state=new Vector<SyncObject>();
				}
				if(state!=null)
					sm.setSyncState(col.newSyncKey, state);
				else
					log.warning("Error saving "+col.newSyncKey+" - no state info");
			}
			xo.close();
			xo.close();
			xo.getBytes();
		}
	}

}
