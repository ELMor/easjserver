package org.olmo.easjs.server.commands.objects;

import java.io.Serializable;
import java.util.Vector;

import org.olmo.easjs.server.object.SyncObject;

public class PingCol implements Serializable {

	private static final long serialVersionUID = -2861869816035812666L;
	
	public String fid=null;
	public String className=null;
	public Vector<SyncObject> state=null;
	
}
