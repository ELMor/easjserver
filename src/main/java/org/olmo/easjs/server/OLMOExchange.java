package org.olmo.easjs.server;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import org.olmo.common.Util;
import org.olmo.easjs.Conf;

public class OLMOExchange {
	StringBuilder responseHeaders=new StringBuilder();
	String requestMethod;
	String fullUrl,url;
	InputStream is=null;
	OutputStream os=null;
	Map<String,String> params=null;
	HashMap<String,String> reqHe=new HashMap<String, String>();
	Logger log=Logger.getLogger(getClass().getSimpleName());
	public String stat=null;
	
	public OLMOExchange(Socket sck){
		try {
			is=sck.getInputStream();
			os=sck.getOutputStream();
			//Lectura del comando y de las cabeceras
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			{
				int i1=-1,i2=0,i3=1,i4=2;
				do{ //Hasta que leamos dos retornos de carro
					i1=i2;
					i2=i3;
					i3=i4;
					i4=is.read();
					if(Conf.debugHttp)
						System.out.print((char)i4);
					if(i4==-1)
						break;
					baos.write(i4);
				}while(i1!=i3 || i3!=13 || i2!=i4 || i4!=10);
			}
			InputStreamReader isr=new InputStreamReader(new ByteArrayInputStream(baos.toByteArray()));
	        BufferedReader reader = new BufferedReader(isr);
	        String line = reader.readLine(); //Metodo, URL y HTTP/1.1
	        {
	        	StringTokenizer stz=new StringTokenizer(line," ");
	        	requestMethod=stz.nextToken(); 
	        	fullUrl=stz.nextToken();
	        	int i=fullUrl.indexOf('?');
	        	if(i>0){
	        		url=fullUrl.substring(0,i);
	        		params=Util.getQueryMap(fullUrl.substring(url.length()+1));
	        	}else
	        		url=fullUrl;
	        }
	        stat=requestMethod+" "+params.get("Cmd");
	        //Headers hasta que haya una linea vac�a
	        while((line=reader.readLine()).length()>0){
	        	int i=line.indexOf(':');
	        	if(i>0){
	        		String header=line.substring(0,i).trim();
	        		reqHe.put(header,line.substring(i+1).trim());
	        	}
	        }
	        //Ahora queda el body, comprobamos content-length
	        String len=reqHe.get("Content-Length");
	        if(len!=null){
	        	int l=Integer.parseInt(len);
	        	byte isbytes[]=new byte[l];
	        	if(l>0)
	        		is.read(isbytes);
	        	is=new ByteArrayInputStream(isbytes);
	        }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void addResponseHeader(String head, String value){
		responseHeaders.append(head).append(":").append(value).append("\n");
	}
	public void sendResponseHeaders(int code, int size) throws IOException{
		responseHeaders.append("HTTP/1.1 ").append(code).append(code==200 ? "OK\n" :"\n");
		if(size>0){
			responseHeaders.append("Content-Length:").append(size).append("\n");
		}
		responseHeaders.append("\n");
		os.write(responseHeaders.toString().getBytes("UTF-8"));
	}
}
